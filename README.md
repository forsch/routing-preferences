# Routing Preferences

* The source code of the depending `de.geoinfobonn.io` can be found [here](https://gitlab.igg.uni-bonn.de/geoinfo/java-io).
* The source code of the depending `org.vgiscience.orfinder` can be found [here](https://gitlab.vgiscience.de/forsch/orfinder).
