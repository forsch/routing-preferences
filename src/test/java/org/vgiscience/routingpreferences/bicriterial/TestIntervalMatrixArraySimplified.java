package org.vgiscience.routingpreferences.bicriterial;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.graph.InvalidGraphWalkException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vgiscience.orfinder.exceptions.AlphaOutsideIntervalException;
import org.vgiscience.orfinder.exceptions.BoundaryLineNotOptimalException;
import org.vgiscience.orfinder.exceptions.InvalidIntervalException;
import org.vgiscience.orfinder.exceptions.MaximumIterationException;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.routingpreferences.datatypes.AttributeFactory;
import org.vgiscience.routingpreferences.graph.BinaryCostFunction;
import org.vgiscience.routingpreferences.graph.FactoredCostFunction;
import org.vgiscience.routingpreferences.graph.FeatureGraphConverter;
import org.vgiscience.routingpreferences.graph.LongCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.DegreeTwoNodeEliminator;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedRoadGraph;
import org.vgiscience.routingpreferences.io.OutputWriter;
import org.vgiscience.routingpreferences.io.ResultSetToShp;
import org.vgiscience.routingpreferences.path.RoadPath;
import org.vgiscience.routingpreferences.path.RoadPathLineProvider;

import de.geoinfobonn.io.FeatureReader;
import de.geoinfobonn.io.format.shp.SHPFeatureReader;
import de.geoinfobonn.io.model.graph.FeatureGraph;
import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class TestIntervalMatrixArraySimplified {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final File nodeFile = new File("src/test/resources/graph/nodes.shp");
	private static final File arcFile = new File("src/test/resources/graph/arcs.shp");
	private static final File pathFile = new File("src/test/resources/path/chunks.shp");
	private static final File outputDir = new File("src/test/resources/output/");

	private static SimplifiedRoadGraph sg;
	private static OutputWriter<String, Integer> outputWriter;
	private static ArrayList<ResultSet> results = new ArrayList<>();

	@BeforeAll
	public static void loadGraph() {
		FeatureGraph fg = FeatureReader.importFeatureGraph(Optional.of(nodeFile), arcFile, null,
				new SHPFeatureReader());

		LinkedList<AttributeFactory<?>> factories = new LinkedList<>();
		factories.add(AttributeFactory.LENGTH);
		factories.add(AttributeFactory.CYCLING);

		RoadGraph g = FeatureGraphConverter.toRoadGraph(fg, factories);
		DegreeTwoNodeEliminator eliminator = new DegreeTwoNodeEliminator(g);
		sg = eliminator.getSimplifiedGraph();

		outputWriter = new OutputWriter<>(g);
		outputWriter.setOutputDirectory(outputDir);
		outputWriter.writeStatsHeader();
	}

	@ParameterizedTest(name = "{index}: {0}, p={1}, s={2}")
	@MethodSource("provideParameters")
	public void testDecompose(IdentifiedPointList<String, Integer> pointList, LongCostFunction primary,
			LongCostFunction secondary) {

		RoadPath path = sg.insertPointListAsPath(pointList);
		path.verify();

		try {
			outputWriter.setCurrentData(pointList.getTrackId(), pointList.getSubtrackId(), pointList.length(),
					"TestIMA");
			outputWriter.setCurrentPath(path);
			RoadPathLineProvider lineProvider = new RoadPathLineProvider(sg, path, primary, secondary);
			ResultSet rs = path.decompose(lineProvider, 0.05);
			outputWriter.writeStats(primary, secondary, rs);
			outputWriter.writeIMA();
			outputWriter.writeDecomposition(rs);
			results.add(rs);
			ArrayList<Pair<Integer, Interval>> multiAlphaDec = path.getIntervalMatrixArray()
					.computeMultipleAlphaDecomposition();
			multiAlphaDec.stream().map(p -> p.getLeft() + ": " + p.getRight()).forEach(logger::fine);
		} catch (MaximumIterationException e) {
			e.printStackTrace();
			Assertions.fail(e);
		} catch (InvalidIntervalException e) {
			e.printStackTrace();
			Assertions.fail(e);
		} catch (BoundaryLineNotOptimalException e) {
			e.printStackTrace();
			Assertions.fail(e);
		} catch (AlphaOutsideIntervalException e) {
			e.printStackTrace();
			Assertions.fail(e);
		} catch (IOException e) {
			e.printStackTrace();
			Assertions.fail(e);
		} catch (InvalidGraphWalkException e) {
			e.printStackTrace();
			Assertions.fail(e);
		}
	}

	@AfterEach
	public void restore() {
		sg.restore();
	}

	@AfterAll
	public static void exportResultSetsToShp() {
		try {
			ResultSetToShp.exportToSHP(outputDir, "TestIntervalMatrix", results);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Stream<Arguments> provideParameters() {
		LinkedList<IdentifiedPointList<String, Integer>> pointLists = FeatureReader.importPointLists(pathFile,
				new SHPFeatureReader());
		LongCostFunction distance = new FactoredCostFunction.Distance();
		BinaryCostFunction cycling_distance = new BinaryCostFunction.Cycling(distance);

		LinkedList<LongCostFunction> costFunctions = new LinkedList<>();
		costFunctions.add(cycling_distance);

		LinkedList<Arguments> arguments = new LinkedList<>();
		for (int pathIndex = 0; pathIndex < pointLists.size(); pathIndex++)
			for (LongCostFunction c : costFunctions)
				// pathIndex:
				// 0 - custom_05, 1
				// 1 - custom_04, 2
				// 2 - custom_04, 1
				// 3 - custom_03, 1
				// 4 - custom_02, 1
				// 5 - custom_01, 1
				// 6 - lptjz... , 1
				if (pathIndex == 4)
					arguments.add(Arguments.of(pointLists.get(pathIndex), distance, c));

		return arguments.stream();
	}
}
