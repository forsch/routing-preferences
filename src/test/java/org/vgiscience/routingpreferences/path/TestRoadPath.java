package org.vgiscience.routingpreferences.path;

import java.io.File;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Stream;

import org.jgrapht.graph.GraphWalk;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.FeatureGraphConverter;
import org.vgiscience.routingpreferences.graph.RoadGraph;

import de.geoinfobonn.io.FeatureReader;
import de.geoinfobonn.io.format.shp.SHPFeatureReader;
import de.geoinfobonn.io.model.graph.FeatureGraph;
import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class TestRoadPath {
//	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final File nodeFile = new File("src/test/resources/graph/nodes.shp");
	private static final File arcFile = new File("src/test/resources/graph/arcs.shp");
	private static final File pathFile = new File("src/test/resources/path/chunks.shp");

	private static RoadGraph g;

	@BeforeAll
	public static void loadGraph() {
		FeatureGraph fg = FeatureReader.importFeatureGraph(Optional.of(nodeFile), arcFile, null,
				new SHPFeatureReader());
		g = FeatureGraphConverter.toRoadGraph(fg, null);
	}

	@ParameterizedTest(name = "{index}: {0}")
	@MethodSource("providePointLists")
	public void testInsert(IdentifiedPointList<String, Integer> pointList) {
		g.addEndpoints(pointList);
//		GraphWalk<AttributedVertex, AttributedEdge> walk = pointList.asWalk(g, v -> new AttributedVertex(v));
		GraphWalk<AttributedVertex, AttributedEdge> walk = PathInserter.pointListAsWalk(pointList, g,
				c -> new AttributedVertex(c));
		walk.verify();

		RoadPath path = g.insertInbetweenNodes(walk, "test");
		path.verify();

		Assertions.assertEquals(pointList.length(), path.length(), 2);
	}

	@AfterEach
	public void restore() {
		g.restore();
	}

	private static Stream<Arguments> providePointLists() {
		LinkedList<IdentifiedPointList<String, Integer>> pointLists = FeatureReader.importPointLists(pathFile,
				new SHPFeatureReader());

		LinkedList<Arguments> arguments = new LinkedList<>();
		for (int pathIndex = 0; pathIndex < pointLists.size(); pathIndex++)
			arguments.add(Arguments.of(pointLists.get(pathIndex)));

		return arguments.stream();
	}
}
