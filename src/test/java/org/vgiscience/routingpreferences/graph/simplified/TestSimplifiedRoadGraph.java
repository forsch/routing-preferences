package org.vgiscience.routingpreferences.graph.simplified;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.LinkedList;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.geotools.referencing.CRS;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.vgiscience.routingpreferences.datatypes.AttributeFactory;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.BinaryCostFunction;
import org.vgiscience.routingpreferences.graph.CombinedDoubleCostFunction;
import org.vgiscience.routingpreferences.graph.FactoredCostFunction;
import org.vgiscience.routingpreferences.graph.FeatureGraphConverter;
import org.vgiscience.routingpreferences.graph.LongCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.graph.WeightedRoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.WeightedSimplifiedRoadGraph.SimplifiedDoubleCostFunction;
import org.vgiscience.routingpreferences.io.PathShpWriter;

import de.geoinfobonn.io.FeatureReader;
import de.geoinfobonn.io.format.shp.SHPFeatureReader;
import de.geoinfobonn.io.model.graph.FeatureGraph;
import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class TestSimplifiedRoadGraph {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final double EPS = 1e-6;

	private static final File nodeFile = new File("src/test/resources/graph/nodes.shp");
	private static final File arcFile = new File("src/test/resources/graph/arcs.shp");
	private static final File pathFile = new File("src/test/resources/path/chunks.shp");
	private static final File outputDir = new File("src/test/resources/output/");

	private static RoadGraph g;
	private static SimplifiedRoadGraph sg;

	public static final CoordinateReferenceSystem crs;
	static {
		CoordinateReferenceSystem c = null;
		try {
			c = CRS.decode("EPSG:3044");
		} catch (FactoryException e) {
			logger.warning("Unable to instantiate crs");
		}
		crs = c;
	}

	@BeforeAll
	public static void loadGraph() {
		FeatureGraph fg = FeatureReader.importFeatureGraph(Optional.of(nodeFile), arcFile, null,
				new SHPFeatureReader());

		LinkedList<AttributeFactory<?>> factories = new LinkedList<>();
		factories.add(AttributeFactory.CYCLING);
		factories.add(AttributeFactory.LENGTH);

		g = FeatureGraphConverter.toRoadGraph(fg, factories);
		logger.info("g.n=" + g.vertexSet().size() + ", g.m=" + g.edgeSet().size());

		DegreeTwoNodeEliminator eliminator = new DegreeTwoNodeEliminator(g);
		sg = eliminator.getSimplifiedGraph();
		logger.info("sg.n=" + sg.vertexSet().size() + ", sg.m=" + sg.edgeSet().size());
	}

	private static Stream<Arguments> provideParameters() {
		FactoredCostFunction distanceCostFunction = new FactoredCostFunction.Distance();
		BinaryCostFunction cyclingCostFunction = new BinaryCostFunction.Cycling(distanceCostFunction);

		LinkedList<Arguments> arguments = new LinkedList<>();
		for (int a = 0; a <= 4; a++) {
			double alpha = a * 0.25;
			switch (a) {
			case 0:
				alpha = 0f;
				break;
			case 1:
				alpha = 1f;
				break;
			case 2:
				alpha = 0.5f;
				break;
			case 3:
				alpha = 0.25f;
				break;
			case 4:
				alpha = 0.75f;
				break;
			}
			arguments.add(Arguments.of(alpha, distanceCostFunction, cyclingCostFunction));
		}
		return arguments.stream();
	}

	@ParameterizedTest(name = "{index}: {0}: {1} - {2}")
	@MethodSource("provideParameters")
	public void testSimplify(double alpha, LongCostFunction primary, LongCostFunction secondary) {
		CombinedDoubleCostFunction combined = new CombinedDoubleCostFunction(alpha, primary, secondary);

		WeightedRoadGraph gw = new WeightedRoadGraph(g, combined);
		WeightedSimplifiedRoadGraph sgw = new WeightedSimplifiedRoadGraph(sg, combined);

		DijkstraShortestPath<AttributedVertex, AttributedEdge> dijkstraRoad = new DijkstraShortestPath<>(gw);
		DijkstraShortestPath<AttributedVertex, SimplifiedEdge> dijkstraSimple = new DijkstraShortestPath<>(sgw);

		for (AttributedVertex source : sg.vertexSet()) {
			var pathsRoad = dijkstraRoad.getPaths(source);
			var pathsSimple = dijkstraSimple.getPaths(source);
			for (AttributedVertex sink : sg.vertexSet()) {
				GraphPath<AttributedVertex, AttributedEdge> pathRoad = pathsRoad.getPath(sink);
				GraphPath<AttributedVertex, SimplifiedEdge> pathSimple = pathsSimple.getPath(sink);
				if (pathRoad == null) {
					Assertions.assertNull(pathSimple);
				} else {
					if (Math.abs(pathRoad.getWeight() - pathSimple.getWeight()) > EPS) {
						System.out.println(alpha + ", " + pathRoad.getWeight() + ", " + pathSimple.getWeight());

						try {
							PathShpWriter.export(outputDir, Optional.of("_rg"), pathRoad, Optional.of(combined),
									Optional.of(new SimplifiedDoubleCostFunction(combined)), Optional.of(crs));
							PathShpWriter.export(outputDir, Optional.of("_sg"), pathSimple, Optional.of(combined),
									Optional.of(new SimplifiedDoubleCostFunction(combined)), Optional.of(crs));
						} catch (IOException e) {
							e.printStackTrace();
						}

						System.out.println("not equal");
					}
					Assertions.assertEquals(pathRoad.getWeight(), pathSimple.getWeight(), EPS,
							"source=" + source + ", sink=" + sink);
				}
			}
			logger.finest("All distances from source=" + source + " are correct.");
		}
	}

	private static Stream<Arguments> provideParametersPath() {
		LinkedList<IdentifiedPointList<String, Integer>> pointLists = FeatureReader.importPointLists(pathFile,
				new SHPFeatureReader());

		FactoredCostFunction distanceCostFunction = new FactoredCostFunction.Distance();
		BinaryCostFunction cyclingCostFunction = new BinaryCostFunction.Cycling(distanceCostFunction);

		LinkedList<Arguments> arguments = new LinkedList<>();
		for (int a = 0; a <= 4; a++) {
			double alpha = a * 0.25;
			switch (a) {
			case 0:
				alpha = 0f;
				break;
			case 1:
				alpha = 1f;
				break;
			case 2:
				alpha = 0.5f;
				break;
			case 3:
				alpha = 0.25f;
				break;
			case 4:
				alpha = 0.75f;
				break;
			}
			for (int pathIndex = 0; pathIndex < pointLists.size(); pathIndex++)
				// pathIndex:
				// 0 - custom_05, 1
				// 1 - custom_04, 2
				// 2 - custom_04, 1
				// 3 - custom_03, 1
				// 4 - custom_02, 1
				// 5 - custom_01, 1
				// 6 - lptjz... , 1
//				if (pathIndex == 5)
				arguments
						.add(Arguments.of(pointLists.get(pathIndex), alpha, distanceCostFunction, cyclingCostFunction));
		}
		return arguments.stream();
	}

	@ParameterizedTest(name = "{index}: {0}, {1}: {2} - {3}")
	@MethodSource("provideParametersPath")
	public void testSimplifyWithPath(IdentifiedPointList<String, Integer> pointList, double alpha,
			LongCostFunction primary, LongCostFunction secondary) {
//		g.exportToShapefile(outputDir, Optional.empty(), Optional.of("_Before"), Optional.empty());

		sg.insertPointListAsPath(pointList);

//		g.exportToShapefile(outputDir, Optional.empty(), Optional.empty(), Optional.empty());
//		sg.exportToShapefile(outputDir, Optional.empty(), Optional.empty(), Optional.empty());

		CombinedDoubleCostFunction combined = new CombinedDoubleCostFunction(alpha, primary, secondary);
		WeightedRoadGraph gw = new WeightedRoadGraph(g, combined);
		WeightedSimplifiedRoadGraph sgw = new WeightedSimplifiedRoadGraph(sg, combined);

		DijkstraShortestPath<AttributedVertex, AttributedEdge> dijkstraRoad = new DijkstraShortestPath<>(gw);
		DijkstraShortestPath<AttributedVertex, SimplifiedEdge> dijkstraSimple = new DijkstraShortestPath<>(sgw);

		for (AttributedVertex source : sg.vertexSet()) {
			var pathsRoad = dijkstraRoad.getPaths(source);
			var pathsSimple = dijkstraSimple.getPaths(source);
			for (AttributedVertex sink : sg.vertexSet()) {
				GraphPath<AttributedVertex, AttributedEdge> pathRoad = pathsRoad.getPath(sink);
				GraphPath<AttributedVertex, SimplifiedEdge> pathSimple = pathsSimple.getPath(sink);
				if (pathRoad == null) {
					Assertions.assertNull(pathSimple);
				} else {
					if (Math.abs(pathRoad.getWeight() - pathSimple.getWeight()) > EPS) {
						System.out.println(alpha + ", " + pathRoad.getWeight() + ", " + pathSimple.getWeight());

						try {
							PathShpWriter.export(outputDir, Optional.of("_rg_" + pointList.getTrackId()), pathRoad,
									Optional.of(combined), Optional.of(new SimplifiedDoubleCostFunction(combined)),
									Optional.of(crs));
							PathShpWriter.export(outputDir, Optional.of("_sg_" + pointList.getTrackId()), pathSimple,
									Optional.of(combined), Optional.of(new SimplifiedDoubleCostFunction(combined)),
									Optional.of(crs));
						} catch (IOException e) {
							e.printStackTrace();
						}

						System.out.println("not equal");
					}
					Assertions.assertEquals(pathRoad.getWeight(), pathSimple.getWeight(), EPS,
							"source=" + source + ", sink=" + sink);
				}
			}
			logger.finest("All distances from source=" + source + " are correct.");
		}

	}

	@AfterEach
	public void restore() {
		sg.restore();
	}
}
