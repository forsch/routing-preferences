package org.vgiscience.routingpreferences;

import java.io.File;
import java.io.FileFilter;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.geotools.referencing.CRS;
import org.jgrapht.graph.GraphWalk;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.vgiscience.routingpreferences.bicriterial.ResultSet;
import org.vgiscience.routingpreferences.datatypes.AttributeFactory;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.BinaryCostFunction;
import org.vgiscience.routingpreferences.graph.FactoredCostFunction;
import org.vgiscience.routingpreferences.graph.FeatureGraphConverter;
import org.vgiscience.routingpreferences.graph.LongCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.DegreeTwoNodeEliminator;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedRoadGraph;
import org.vgiscience.routingpreferences.io.OutputWriter;
import org.vgiscience.routingpreferences.io.ResultSetToShp;
import org.vgiscience.routingpreferences.path.PathInserter;
import org.vgiscience.routingpreferences.path.RoadPath;
import org.vgiscience.routingpreferences.path.RoadPathLineProvider;

import de.geoinfobonn.io.FeatureReader;
import de.geoinfobonn.io.format.shp.SHPFeatureReader;
import de.geoinfobonn.io.model.graph.FeatureGraph;
import de.geoinfobonn.io.model.path.IdentifiedPointList;

/**
 * The application to evaluate routing preferences in a bicriterial setting.
 */
public class BicriterialApp {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	/** The default coordinate reference system to export results to. */
	public static final CoordinateReferenceSystem crs;
	static {
		CoordinateReferenceSystem c = null;
		try {
			c = CRS.decode("EPSG:3044");
		} catch (FactoryException e) {
			logger.warning("Unable to instantiate crs due to `" + e + "`");
		}
		crs = c;
	}

	/**
	 * Run.
	 *
	 * @param roadShp            a LineString-Shapefile containing the road segments
	 * @param nodeShp            an optional Point-Shapefile containing all
	 *                           end-points contained in {@code roadShp}
	 * @param pathShps           a list of LineString-Shapefiles, each containing
	 *                           paths to analyze
	 * @param outDir             directory for storing output
	 * @param minLength          minimum length of a path to be analyzed (in meters)
	 * @param attributeFactories factories to convert Shapefile fields to graph
	 *                           attributes
	 * @param costFunctions      list containing the two cost-functions to evaluate
	 * @param basic              if the basic graph (instead of the simplified
	 *                           graph) should be used, slower but more consistent
	 */
	public static void run(File roadShp, Optional<File> nodeShp, List<File> pathShps, File outDir, double minLength,
			double maxLength, List<AttributeFactory<?>> attributeFactories, List<LongCostFunction> costFunctions,
			boolean basic) {
		logConfig(roadShp, nodeShp, pathShps, outDir, minLength, maxLength, basic);
		checkInput(roadShp, nodeShp, pathShps, outDir, minLength, maxLength, basic);

		LongCostFunction primary = costFunctions.get(0);
		LongCostFunction secondary = costFunctions.get(1);

		RoadGraph rg = null;

		FeatureGraph fg = FeatureReader.importFeatureGraph(nodeShp, roadShp, null, new SHPFeatureReader());
		rg = FeatureGraphConverter.toRoadGraph(fg, attributeFactories);
		fg = null;
		logHeapSpace("Road Graph");

		SimplifiedRoadGraph sg = null;
		if (!basic) {
			DegreeTwoNodeEliminator eliminator = new DegreeTwoNodeEliminator(rg);
			sg = eliminator.getSimplifiedGraph();
			logHeapSpace("Simplified Road Graph");
		}

		OutputWriter<String, Integer> outputWriter = new OutputWriter<>(rg);
		outputWriter.setOutputDirectory(outDir);
		outputWriter.writeStatsHeader();

		int fileIdx = 0;
		for (File pathShp : pathShps) {
			LinkedList<IdentifiedPointList<String, Integer>> pointLists;
			ArrayList<ResultSet> results;
			try {
				pointLists = FeatureReader.importPointLists(pathShp, new SHPFeatureReader());
				results = new ArrayList<>(pointLists.size());
				logger.fine("Starting with " + pathShp + " (" + ++fileIdx + "/" + pathShps.size() + "), found "
						+ pointLists.size() + " paths.");
			} catch (Exception e) {
				logger.severe("Encountered `" + e + "` during loading " + pathShp.getName());
				continue;
			}

			for (int i = 0; i < pointLists.size(); ++i) {
				IdentifiedPointList<String, Integer> pointList = pointLists.get(i);
				outputWriter.setCurrentData(pointList.getTrackId(), pointList.getSubtrackId(), pointList.length(),
						"BicriterialApp");

				RoadPath path;
				try {
					logger.fine("Starting subpath " + i + " (trackId=" + pointList.getTrackId() + ", subtrackId="
							+ pointList.getSubtrackId() + ")");

					if (pointList.length() < minLength) {
						String message = String.format("Skipping path due to minLength constraint (minLength=%.2f)",
								minLength);
						logger.fine(message);
						outputWriter.writeFailMessage(message);
						continue;
					}

					if (pointList.length() > maxLength) {
						String message = String.format("Skipping path due to maxLength constraint (maxLength=%.2f)",
								maxLength);
						logger.fine(message);
						outputWriter.writeFailMessage(message);
						continue;
					}

					if (basic) {
						rg.addEndpoints(pointList);
						GraphWalk<AttributedVertex, AttributedEdge> walk = PathInserter.pointListAsWalk(pointList, rg,
								c -> new AttributedVertex(c));
						walk.verify();

						String pathId = pointList.getTrackId() + "_" + pointList.getSubtrackId();
						path = rg.insertInbetweenNodes(walk, pathId);
					} else {
						path = sg.insertPointListAsPath(pointList);
					}
					path.verify();
					logger.fine("Path insert done, size=" + path.getLength() + ", length=" + path.length());
				} catch (Exception e) {
//					e.printStackTrace();
					outputWriter.writeFailMessage(e);
					logger.severe("Encountered `" + e + "` during path insertion.");
					if (basic)
						rg.restore();
					else
						sg.restore();
					continue;
				}

				try {
					outputWriter.setCurrentPath(path);

					RoadPathLineProvider lineProvider;
					if (basic)
						lineProvider = new RoadPathLineProvider(rg, path, primary, secondary);
					else
						lineProvider = new RoadPathLineProvider(sg, path, primary, secondary);

					ResultSet rs = path.decompose(lineProvider, 0.05);
					results.add(rs);
					outputWriter.writeStats(primary, secondary, rs);
					outputWriter.writeIMA();
					outputWriter.writeDecomposition(rs);
				} catch (Exception e) {
//					e.printStackTrace();
					outputWriter.writeFailMessage(e);
					logger.severe("Encountered `" + e + "` during path insertion.");
				}

				if (basic)
					rg.restore();
				else
					sg.restore();
			}

			try {
				File suiteFile = new File(outDir, "suites/");
				suiteFile.mkdir();
				ResultSetToShp.exportMilestoneSuites(suiteFile,
						pathShp.getName().substring(0, pathShp.getName().length() - 4), results,
						Optional.ofNullable(crs));
			} catch (Exception e) {
				logger.warning("Encountered `" + e + "` during exporting milestone suites.");
			}
		}
		logger.info("Done!");
	}

	/**
	 * Logs the run configuration.
	 *
	 * @param roadShp   the road Shapefile
	 * @param nodeShp   the node Shapefile
	 * @param pathShps  the path Shapefiles
	 * @param outDir    the output directory
	 * @param minLength the minimum length of paths to be analyzed
	 * @param basic     whether the basic graph should be used
	 */
	private static void logConfig(File roadShp, Optional<File> nodeShp, List<File> pathShps, File outDir,
			double minLength, double maxLength, boolean basic) {
		logger.config(String.format("%-20s %s", "Road shapefile:", roadShp));
		logger.config(String.format("%-20s %s", "Node shapefile:", nodeShp.isPresent() ? nodeShp.get() : "-"));
		logger.config(String.format("%-20s %d", "Path shapefile(s):", pathShps.size()));
		pathShps.stream().limit(10).forEach(pathShp -> logger.config(String.format("  - %s", pathShp)));
		logger.config(String.format("%-20s %s", "Output directory:", outDir));
		logger.config(String.format("%-20s %.1f", "Minimum path length:", minLength));
		logger.config(String.format("%-20s %.1f", "Maximum path length:", maxLength));
		logger.config(String.format("%-20s %b", "Basic graph", basic));
	}

	/**
	 * Checks the input parameters for the run method. Throws an error in case a
	 * parameter is invalid.
	 *
	 * @param roadShp   the road Shapefile
	 * @param nodeShp   the node Shapefile
	 * @param pathShps  the path Shapefiles
	 * @param outDir    the output directory
	 * @param minLength the minimum length of paths to be analyzed
	 * @param basic     whether the basic graph should be used
	 */
	private static void checkInput(File roadShp, Optional<File> nodeShp, List<File> pathShps, File outDir,
			double minLength, double maxLength, boolean basic) {
		// input check
		Objects.requireNonNull(roadShp);
		Objects.requireNonNull(pathShps);
		Objects.requireNonNull(outDir);
		if (pathShps.isEmpty())
			throw new IllegalArgumentException("Empty pathShps, aborting");
		checkForShp("roadShp", roadShp);
		if (nodeShp.isPresent())
			checkForShp("nodeShp", nodeShp.get());
		checkForShp("pathShps", pathShps.toArray(File[]::new));
		if (minLength < 0)
			throw new IllegalArgumentException("minimum length must be non-negative");
		if (maxLength < minLength)
			throw new IllegalArgumentException("maximum length must be greater than minimum length");
	}

	/**
	 * Checks the provided files for being Shapefiles.
	 *
	 * @param name  name identifying the group of files
	 * @param files the files to check
	 * @throws IllegalArgumentException in case one of the files is not a Shapefile
	 */
	private static void checkForShp(String name, File... files) {
		if (Arrays.stream(files).anyMatch(p -> !p.getName().endsWith(".shp")))
			throw new IllegalArgumentException(name + " must be shapefiles!");
		if (Arrays.stream(files).anyMatch(p -> !p.exists()))
			throw new IllegalArgumentException(name + " must exist!");
	}

	/**
	 * The main entrance point for the {@link BicriterialApp}.
	 *
	 * @param args the (program) arguments
	 * @throws ParseException in case the arguments cannot be parsed
	 */
	public static final void main(String[] args) throws ParseException {
		Option help = new Option("h", "help", false, "Shows this help");
		Option version = new Option("v", "version", false, "Prints the version number");

		OptionGroup pathInput = new OptionGroup();
		pathInput.addOption(Option.builder("p").longOpt("path").hasArg(true).desc("Path shapefile").build());
		pathInput.addOption(Option.builder("d").longOpt("pathDirectory").hasArg(true)
				.desc("Directory containing path shapefiles").build());
		pathInput.setRequired(true);

		Options options = new Options();
		options.addOption(help);
		options.addOption(version);
		options.addRequiredOption("r", "roads", true, "Road graph shapefile");
		options.addOption("n", "nodes", true,
				"Shapefile containing all noded intersections of the road graph with information");
		options.addOption("o", "output", true, "Output Directory, default: out/");
		options.addOptionGroup(pathInput);
		options.addOption("l", "minLength", true, "Minimum length (in meters) of a path to be analyzed, default: 0");
		options.addOption("L", "maxLength", true, "Maximum length (in meters) of a path to be analyzed, default: INF");
		options.addRequiredOption("P", "primary", true, "Primary Cost Function");
		options.addRequiredOption("S", "secondary", true, "Secondary Cost Function");
		options.addOption("b", "basic", false, "Use basic, non-simplified graph (more robust but slow).");

		// check for meta options such as help and version
		Options metaOptions = new Options();
		metaOptions.addOption(help);
		metaOptions.addOption(version);

		CommandLine metaCL = new DefaultParser().parse(metaOptions, args, true);
		if (metaCL.getOptions().length != 0) {
			if (metaCL.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(MethodHandles.lookup().lookupClass().getName(), options);
			}

			if (metaCL.hasOption("v")) {
				logger.config("Version 1.0.0 - TODO!");
			}

			System.exit(0);
		}

		// parse program options
		CommandLineParser parser = new DefaultParser();
		CommandLine cl = parser.parse(options, args);

		File roadShp = new File(cl.getOptionValue("r"));
		Optional<File> nodeShp = cl.hasOption("n") ? Optional.of(new File(cl.getOptionValue("n"))) : Optional.empty();
		LinkedList<File> pathShps = new LinkedList<>();
		if (cl.hasOption("p"))
			pathShps.add(new File(cl.getOptionValue("p")));
		else
			pathShps.addAll(getShapefilesIn(cl.getOptionValue("d")));
		File outDir = new File(cl.getOptionValue("o", "out"));
		double minLength = Double.parseDouble(cl.getOptionValue("l", "0"));
		double maxLength = Double.parseDouble(cl.getOptionValue("L", "Infinity"));

		LinkedList<AttributeFactory<?>> attributeFactories = new LinkedList<>();
		attributeFactories.add(getAttributeFactory(cl.getOptionValue("P")));
		attributeFactories.add(getAttributeFactory(cl.getOptionValue("S")));

		LinkedList<LongCostFunction> costFunctions = new LinkedList<>();
		costFunctions.add(getCostFunction(cl.getOptionValue("P"), null));
		costFunctions.add(getCostFunction(cl.getOptionValue("S"), Optional.of(costFunctions.getFirst())));

		boolean basic = cl.hasOption("b");

		BicriterialApp.run(roadShp, nodeShp, pathShps, outDir, minLength, maxLength, attributeFactories, costFunctions,
				basic);
	}

	/**
	 * Gets the attribute factory based on the given tag.
	 *
	 * @param tag the tag of the attribute factory
	 * @return the attribute factory
	 */
	private static AttributeFactory<?> getAttributeFactory(String tag) {
		switch (tag) {
		case "ascent":
			return AttributeFactory.ASCENT;
		case "cycling":
			return AttributeFactory.CYCLING;
		case "length":
			return AttributeFactory.LENGTH;
		case "surface":
			return AttributeFactory.SURFACE;
		default:
			throw new IllegalArgumentException("Unknown tag queried");
		}
	}

	/**
	 * Gets the cost function based on the given tag. For tags related to binary
	 * cost functions, the corresponding {@code primary} cost function must be
	 * supplied.
	 *
	 * @param tag     the tag of the cost function
	 * @param primary the primary cost function in case of tags belonging to binary
	 *                cost functions
	 * @return the cost function
	 */
	private static LongCostFunction getCostFunction(String tag, Optional<LongCostFunction> primary) {
		switch (tag) {
		case "ascent":
			return new FactoredCostFunction.Ascent();
		case "cycling":
			return new BinaryCostFunction.Cycling(
					primary.orElseThrow(() -> new RuntimeException("primary cost function must be supplied"))//
			);
		case "length":
			return new FactoredCostFunction.Distance();
		case "surface":
			logger.warning("Surface cost function not implemented yet");
		default:
			throw new IllegalArgumentException("Unknown attribute factory queried");
		}
	}

	/**
	 * Returns a list of all shapefiles in the given directory. The resulting list
	 * is sorted by filename in alphabetical order.
	 *
	 * @param directory the directory to search Shapefiles in
	 * @return the shapefiles in {@code directory}
	 */
	private static List<File> getShapefilesIn(String directory) {
		File dir = new File(directory);
		LinkedList<File> files = new LinkedList<>(Arrays.asList(dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if (!pathname.getName().endsWith(".shp"))
					return false;
				return true;
			}
		})));
		Collections.sort(files);
		return files;
	}

	public static void logHeapSpace(String message) {
		int bytes_per_gb = 1024 * 1024 * 1024;

		double freeMemoryInGB = (double) Runtime.getRuntime().freeMemory() / bytes_per_gb;
		double totalMemoryInGB = (double) Runtime.getRuntime().totalMemory() / bytes_per_gb;
		double maxMemoryInGB = (double) Runtime.getRuntime().maxMemory() / bytes_per_gb;

		String msg = String.format(//
				"%s: free=%.3fgb, total=%.3fgb, max=%.3fgb", //
				message, //
				freeMemoryInGB, //
				totalMemoryInGB, //
				maxMemoryInGB//
		);
		logger.config(msg);
	}
}
