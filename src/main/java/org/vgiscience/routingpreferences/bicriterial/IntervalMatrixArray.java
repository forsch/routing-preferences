package org.vgiscience.routingpreferences.bicriterial;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.vgiscience.orfinder.LowerEnvelopeFinder;
import org.vgiscience.orfinder.exceptions.AlphaOutsideIntervalException;
import org.vgiscience.orfinder.exceptions.BoundaryLineNotOptimalException;
import org.vgiscience.orfinder.exceptions.InvalidIntervalException;
import org.vgiscience.orfinder.exceptions.MaximumIterationException;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.orfinder.structures.QueryStat;
import org.vgiscience.routingpreferences.bicriterial.AlphaMilestoneSuites.MilestoneSuite;
import org.vgiscience.routingpreferences.path.RoadPath;
import org.vgiscience.routingpreferences.path.RoadPathLineProvider;

/**
 * Calculates for every sub-path of the given input path the optimality range.
 * The optimality ranges are stored in a 2-dimensional array structured in the
 * following way: The row index indicates the index of the source node of the
 * sub-path and the column index indicates the index of the target node of the
 * sub-path. Therefore the element m[1][4] contains the optimality range of the
 * sub-path P_{1,4} of P, starting at the second node (index = 1) of P and
 * ending at the 5th node of P.
 * 
 * @author Johannes Oehrlein and Axel Forsch
 *
 * @param <E>
 */
public class IntervalMatrixArray {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
	private static final double loggingInterval = .1;

	public Interval[][] m;
	private QueryStat[][] stats;

	private Interval searchInterval;

	private RoadPath p;
	private int n;

	private RoadPathLineProvider lp;

	// debugging variables
	private static int currStop, currStart;
	public static boolean finestIsFiner = false;

	// timing
	private long algorithmTime;
	private long dijkstraTime;
	private int countTotal;
	private int countNone;
	private int countIsOptimal;
	private int countPointOnLE;
	private int countBounds;

	private TreeMap<Double, AlphaMilestoneSuites> suites;

	public IntervalMatrixArray(RoadPathLineProvider lp, RoadPath p) {
		this(lp, p, new Interval());
	}

	public IntervalMatrixArray(RoadPathLineProvider lp, RoadPath p, Interval searchInterval) {
		this.lp = lp;
		this.p = p;
		this.searchInterval = searchInterval;

		n = p.getLength() + 1;

		m = new Interval[n][n];
		stats = new QueryStat[n][n];
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j)
				stats[i][j] = new QueryStat();
	}

	/**
	 * Fill matrix.
	 *
	 * @throws MaximumIterationException       the maximum iteration exception
	 * @throws InvalidIntervalException        the invalid interval exception
	 * @throws BoundaryLineNotOptimalException the boundary line not optimal
	 *                                         exception
	 * @throws AlphaOutsideIntervalException   the alpha outside interval exception
	 */
	private void fillMatrix() throws MaximumIterationException, InvalidIntervalException,
			BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		double perc = loggingInterval; // next percentage for logging
		int done = 0; // number of filled elements
		int todo = (n - 1) * n / 2; // total number of elements to fill
		logger.fine("Starting to fill interval matrix");
		for (int r = n - 1; r >= 0; --r) {
			fillRow(r);

			done += n - r - 1;
			if ((double) done / todo >= perc) {
				logger.fine((int) Math.round((double) done / todo * 100) + "% of interval matrix filled");
				perc += loggingInterval;
			}
		}
	}

	private void fillRow(int r) throws MaximumIterationException, InvalidIntervalException,
			BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		m[r][r] = new Interval(searchInterval);
		currStart = r;

		LowerEnvelopeFinder<RoadPath> lef = new LowerEnvelopeFinder<>(lp);

		lp.setQuery(p);

		for (int c = r + 1; c < n; ++c) {
			QueryStat currStats = stats[r][c];
			long time = System.currentTimeMillis();

			currStop = c;

			RoadPath sp = p.subPath(r, c + 1);

			// m[r + 1][c] and m[r][c - 1] are optimality ranges for sub-paths of P_{r,c}

			// Optimization A:
			// if one of them has an empty optimality range, P_{r,c} has aswell due to
			// monotonicity (Definition 4.2)
			if (m[r + 1][c] == null || m[r][c - 1] == null) {
				m[r][c] = null;
				break;
			}

			// Optimization B:
			// as ALL sub-paths of P_{r,c} have at least the optimality range P_{r,c} has,
			// the search can be restricted to the interval all sub-paths cover
			Interval boundsDueToMonotoneOptimality = m[r + 1][c].intersect(m[r][c - 1]);

			if (boundsDueToMonotoneOptimality == null) {
				m[r][c] = null;
				break;
			}

			logger.finer("Filling element m[" + r + ", " + c + "], bounds from monotonicity: "
					+ boundsDueToMonotoneOptimality);

			lp.setQuery(sp);
			logger.finest("query path: " + sp);

			if (r == 17 && c == 21) {
				IntervalMatrixArray.finestIsFiner = true;
			}

			if (IntervalMatrixArray.finestIsFiner) {
				logger.finer("query path: " + sp.toLineString());
			}

			m[r][c] = lef.findBounds(boundsDueToMonotoneOptimality, Optional.of(currStats));

			if (m[r][c] == null)
				break;

			// collect stats
			time = System.currentTimeMillis() - time;
		}
	}

	public void decompose() throws MaximumIterationException, InvalidIntervalException, BoundaryLineNotOptimalException,
			AlphaOutsideIntervalException {

		long tStart = System.currentTimeMillis();

		// step 1
		fillMatrix();

		long tMatrix = System.currentTimeMillis();
		logger.fine("filled matrix in " + (tMatrix - tStart) / 1000.0 + "s");

		// step 2
		ArrayList<AlphaDecomposition> decomsForward = computeDecompositions(true);
		ArrayList<AlphaDecomposition> decomsBackward = computeDecompositions(false);
		computeMilestoneSuites(decomsForward, decomsBackward);

		long tTraverse = System.currentTimeMillis();
		logger.fine("traversed matrix in " + (tTraverse - tMatrix) / 1000.0 + "s");

		collectStats();

		algorithmTime = tTraverse - tStart;
		logger.fine("total decompose done in " + algorithmTime / 1000.0 + "s");
	}

	/**
	 * Compute milestone suites.
	 *
	 * @param decompositions         the decompositions
	 * @param decompositionsBackward the decompositions backward
	 */
	private void computeMilestoneSuites(ArrayList<AlphaDecomposition> decompositions,
			ArrayList<AlphaDecomposition> decompositionsBackward) {
		suites = new TreeMap<>();
		TreeSet<AlphaDecomposition> fDecoms = new TreeSet<>(alphaComparator);
		for (AlphaDecomposition decom : decompositions) {
			if (fDecoms.contains(decom)) {
				logger.warning(decom + " is already in decoms (" + fDecoms.ceiling(decom) + ")");
				if (decom.isBound)
					fDecoms.remove(decom);
			}
			fDecoms.add(decom);
		}

		TreeSet<AlphaDecomposition> bDecoms = new TreeSet<>(alphaComparator);
		for (AlphaDecomposition decom : decompositionsBackward) {
			if (bDecoms.contains(decom)) {
				logger.warning(decom + " is already in decoms (" + bDecoms.ceiling(decom) + ")");
				if (decom.isBound)
					bDecoms.remove(decom);
			}
			bDecoms.add(decom);
		}

		Iterator<AlphaDecomposition> forward = fDecoms.iterator();
		Iterator<AlphaDecomposition> backward = bDecoms.iterator();
		while (forward.hasNext()) {
			AlphaDecomposition f = forward.next();
			AlphaDecomposition b = backward.next();
			if (f.getAlpha() != b.getAlpha())
				throw new RuntimeException("f and b must have same alpha");
			ArrayList<Integer> fd = f.getDecomposition();
			ArrayList<Integer> bd = b.getDecomposition();
			if (fd.size() != bd.size())
				throw new RuntimeException("fd and bd must be same size");
			int size = fd.size();
			// special case: no optimal option!
			if (fd.size() == 0) {
				throw new RuntimeException("Non optimal solution!");
			}
			// special case: no milestone needed
			if (fd.size() == 1) {
				if (!suites.containsKey(f.getAlpha()))
					suites.put(f.getAlpha(), new AlphaMilestoneSuites(p.getId(), f.alpha, f.isBound));
			}
			for (int i = 0; i < size - 1; ++i) {
				int fIdx = f.getDecomposition().get(i);
				int bIdx = b.getDecomposition().get(size - i - 2);
				if (!suites.containsKey(f.getAlpha()))
					suites.put(f.getAlpha(), new AlphaMilestoneSuites(p.getId(), f.alpha, f.isBound));
				AlphaMilestoneSuites as = suites.get(f.getAlpha());
				as.addSuite(new MilestoneSuite(as, fIdx, bIdx, p.getVertexList()));
			}
		}
	}

	private static Comparator<AlphaDecomposition> alphaComparator = new Comparator<>() {
		@Override
		public int compare(AlphaDecomposition o1, AlphaDecomposition o2) {
			if (o1.alpha == o2.alpha)
				return 0;
			if (o1.alpha > o2.alpha)
				return 1;
			return -1;
		}
	};

	private void collectStats() {
		countTotal = 0;
		countNone = 0;
		countIsOptimal = 0;
		countPointOnLE = 0;
		countBounds = 0;
		dijkstraTime = 0;

		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				countTotal += stats[i][j].getTotalCount();
				countNone += stats[i][j].getCountNoMethod();
				countIsOptimal += stats[i][j].getCountIsOptimal();
				countPointOnLE += stats[i][j].getCountPointOnLE();
				countBounds += stats[i][j].getCountBounds();
				dijkstraTime += stats[i][j].getDijkstraTime();
			}
		}

		logger.finer("Total #dijkstra: " + countTotal);
		logger.finer("  none:          " + countNone);
		logger.finer("  isOptimal:     " + countIsOptimal);
		logger.finer("  pointOnLE:     " + countPointOnLE);
		logger.finer("  findBounds:    " + countBounds);
	}

	private double[] getIntervalBounds() {
		TreeSet<Double> bounds = new TreeSet<Double>();
		for (int r = 0; r < n; ++r) {
			for (int c = r; c < n; ++c) {
				if (m[r][c] != null) {
					bounds.add(m[r][c].getLowerBound());
					bounds.add(m[r][c].getUpperBound());
				} else {
					break;
				}
			}
		}
		Double[] D = bounds.toArray(new Double[bounds.size()]);
		double[] d = new double[bounds.size()];
		for (int i = 0; i < d.length; i++) {
			d[i] = D[i];
		}
		return d;
	}

	private AlphaDecomposition traverseMatrixForwards(double alpha, boolean isBound) {
		int r = 0;
		int c = 0;
		ArrayList<Integer> decomposition = new ArrayList<>();
		while (true) {
			while (c < n) {
				if (m[r][c] != null && m[r][c].contains(alpha)) {
					++c;
				} else {
					--c;
					break;
				}
			}
			if (r == c) {
				decomposition = new ArrayList<Integer>();
				break;
			}
			if (c == n) {
				decomposition.add(n - 1);
				break;
			}
			decomposition.add(c);
			r = c;

		}
		return new AlphaDecomposition(alpha, decomposition, isBound);
	}

	private AlphaDecomposition traverseMatrixBackwards(double alpha, boolean isBound) {
		int r = n - 1;
		int c = n - 1;
		ArrayList<Integer> decomposition = new ArrayList<>();
		while (true) {
			while (r >= 0) {
				if (m[r][c] != null && m[r][c].contains(alpha)) {
					--r;
				} else {
					++r;
					break;
				}
			}
			if (r == c) {
				decomposition = new ArrayList<Integer>();
				break;
			}
			if (r == -1) {
				decomposition.add(0);
				break;
			}
			decomposition.add(r);
			c = r;

		}
		return new AlphaDecomposition(alpha, decomposition, isBound);
	}

	public ArrayList<Pair<Integer, Interval>> computeMultipleAlphaDecomposition() {
		int r = 0;
		int c = 0;
		ArrayList<Pair<Integer, Interval>> decomposition = new ArrayList<>();
		while (true) {
			Interval currentInterval = new Interval();
			while (c < n) {
				if (m[r][c] != null && m[r][c].intersects(currentInterval)) {
					currentInterval = currentInterval.intersect(m[r][c]);
					++c;
				} else {
					--c;
					break;
				}
			}
			if (r == c) {
				decomposition = new ArrayList<>();
				break;
			}
			if (c == n) {
				decomposition.add(new ImmutablePair<>(n - 1, currentInterval));
				break;
			}
			decomposition.add(new ImmutablePair<>(c, currentInterval));
			r = c;

		}
		return decomposition;
	}

	private ArrayList<AlphaDecomposition> computeDecompositions(boolean forward) {
		ArrayList<AlphaDecomposition> decoms = new ArrayList<>();

		boolean added05 = false;

		double[] bounds = getIntervalBounds();
		for (int bi = 0; bi < bounds.length; bi++) {
			if (bi > 0) {
				double rn = 0.5 * (bounds[bi] + bounds[bi - 1]);
				AlphaDecomposition decomposition = forward ? //
						traverseMatrixForwards(rn, false) : //
						traverseMatrixBackwards(rn, false);
				decoms.add(decomposition);
				if (rn == 0.5)
					added05 = true;
			}

			AlphaDecomposition decomposition = forward ? //
					traverseMatrixForwards(bounds[bi], true) : //
					traverseMatrixBackwards(bounds[bi], true);
			decoms.add(decomposition);
			if (bounds[bi] == 0.5)
				added05 = true;
		}

		if (!added05) {
			AlphaDecomposition decomposition = forward ? //
					traverseMatrixForwards(0.5, false) : //
					traverseMatrixBackwards(0.5, false);
			decoms.add(decomposition);
		}

		return decoms;
	}

	public void export(String fname) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(fname));

		// column headers
		bw.write(";");
		for (int c = 0; c < n; c++) {
			bw.write(c + ";");
		}
		bw.write("\n");

		for (int r = 0; r < n; r++) {
			// row "header"
			bw.write(r + ";");

			for (int c = 0; c < n; c++) {
				if (c >= r) {
					Interval i = m[r][c];
					if (i == null) {
						bw.write("-");
					} else {
						bw.write("[" + i.getLowerBound() + "," + i.getUpperBound() + "]");
					}
				}
				bw.write(";");
			}
			bw.write("\n");
		}
		bw.close();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		for (int c = 0; c < n; c++) {
			for (int r = 0; r < n; r++) {
				Interval i = m[r][c];
				sb.append("(" + r + "|" + c + ")");
				if (i == null) {
					sb.append(";");
				} else {
					sb.append(i.getLowerBound() + ";" + i.getUpperBound() + ";");
				}
			}
			sb.append("\n");
		}

		return sb.toString();
	}

	public void printMatrix(Integer iFirst, Integer iSecond) {
		StringBuffer sb = new StringBuffer();

		if (iFirst != null && iSecond != null) {
			sb.append("Cost functions: " + iFirst + " and " + iSecond + "\n");
		}

		sb.append("   | ");
		for (int c = 0; c < n; c++) {
			sb.append("     " + String.format("%2d", c) + "      ");
		}
		sb.append("\n---+-");
		for (int c = 0; c < n; c++) {
			sb.append("-------------");
		}
		sb.append("\n");
		for (int r = 0; r < n; r++) {
			sb.append(String.format("%2d", r) + " | ");
			for (int c = 0; c < n; c++) {
				if (c < r) {
					sb.append("             ");
					continue;
				}
				Interval i = m[r][c];
				if (i == null) {
					sb.append("     [-]     ");
				} else if (i.getLowerBound() == 0 && i.getUpperBound() == 1) {
					sb.append("    full     ");
				} else {
					sb.append(" [" + String.format("%4.2f", i.getLowerBound()) + ","
							+ String.format("%4.2f", i.getUpperBound()) + "] ");
				}
			}
			sb.append("\n");
		}

		System.out.println(sb.toString());
	}

	public void printMatrixLatex(Integer iFirst, Integer iSecond, PrintStream ps) {
		StringBuffer sb = new StringBuffer();

		sb.append("\\begin{tabular}{");
		for (int c = 0; c <= n; c++) {
			sb.append("c");
		}
		sb.append("}\n");

		sb.append("color (index)");
		for (int c = 0; c < n; c++) {
			sb.append(" & " + c);
		}
		sb.append("\\\\\\hline\n");

		for (int r = 0; r < n; r++) {
			sb.append(r);
			for (int c = 0; c < n; c++) {
				sb.append(" & ");
				if (c <= r) {
					Interval i = m[r][c];
					if (i == null) {
						sb.append("[-]");
					} else {
						sb.append("[" + String.format("%4.2f", i.getLowerBound()).replace(".00", "") + ","
								+ String.format("%4.2f", i.getUpperBound()).replace(".00", "") + "]");
					}
				}
			}

			if (r < n - 1)
				sb.append("\\\\\n");
			else
				sb.append("\n");
		}

		sb.append("\\end{tabular}");

		ps.println(sb.toString());
	}

	public void printMatrix() {
		printMatrix(null, null);
	}

	public int getNumDijkstraCalls() {
		return countTotal;
	}

	public int getCountNone() {
		return countNone;
	}

	public int getCountIsOptimal() {
		return countIsOptimal;
	}

	public int getCountPointOnLE() {
		return countPointOnLE;
	}

	public int getCountBounds() {
		return countBounds;
	}

	public long getAlgoTime() {
		return algorithmTime;
	}

	public long getDijkstraTime() {
		return dijkstraTime;
	}

	public TreeMap<Double, AlphaMilestoneSuites> getSuites() {
		return suites;
	}

	public static String locateError() {
		return "ima[" + currStart + "," + currStop + "]";
	}
}
