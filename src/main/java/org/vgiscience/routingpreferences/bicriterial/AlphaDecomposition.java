package org.vgiscience.routingpreferences.bicriterial;

import java.util.ArrayList;
import java.util.Collections;

public class AlphaDecomposition implements Comparable<AlphaDecomposition> {
	double alpha;
	ArrayList<Integer> decomposition;
	boolean isBound;

	public AlphaDecomposition(double alpha, ArrayList<Integer> decomposition, boolean isBound) {
		this.alpha = alpha;
		this.decomposition = decomposition;
		this.isBound = isBound;
	}

	public double getAlpha() {
		return alpha;
	}

	public ArrayList<Integer> getDecomposition() {
		return decomposition;
	}

	public int size() {
		return decomposition.size();
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public void setDecomposition(ArrayList<Integer> decomposition) {
		this.decomposition = decomposition;
	}

	/**
	 * This AlphaDecomposition is smaller than another AlphaDecomposition if it has
	 * less sub paths or if it has the same number but cuts off early the first path
	 * differing
	 */
	@Override
	public int compareTo(AlphaDecomposition o) {
		if (size() < o.size()) {
			return -1;
		}
		if (size() > o.size()) {
			return 1;
		}
		for (int i = 0; i < size(); i++) {
			if (decomposition.get(i) < o.decomposition.get(i)) {
				return -1;
			}
			if (decomposition.get(i) > o.decomposition.get(i)) {
				return 1;
			}
			if (isBound() && !o.isBound()) {
				return -1;
			}
			if (!isBound() && o.isBound()) {
				return 1;
			}
		}
		return 0;
	}

	public boolean isBound() {
		return isBound;
	}

	@Override
	public String toString() {
		String old = "[alpha=" + String.format("%6.4f", alpha) + ", size=" + String.format("%2d", decomposition.size())
				+ ", " + (isBound ? "isB" : "noB");
		StringBuilder sb = new StringBuilder(old);
		sb.append("; ");
		for (int i = 0; i < decomposition.size(); i++) {
			sb.append(decomposition.get(i));
			if (i != decomposition.size() - 1)
				sb.append(", ");
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof AlphaDecomposition))
			return false;

		AlphaDecomposition other = ((AlphaDecomposition) obj);

		if (this.alpha != other.alpha)
			return false;

		if (this.isBound != other.isBound)
			return false;

		if (this.decomposition.size() != other.decomposition.size())
			return false;

		ArrayList<Integer> sortedDecom1 = new ArrayList<>(this.decomposition);
		Collections.sort(sortedDecom1);

		ArrayList<Integer> sortedDecom2 = new ArrayList<>(other.decomposition);
		Collections.sort(sortedDecom2);

		for (int i = 0; i < sortedDecom1.size(); i++) {
			if (sortedDecom1.get(i) != sortedDecom2.get(i)) {
				return false;
			}
		}

		return true;
	}

	// FIXME missing hashCode() function
}
