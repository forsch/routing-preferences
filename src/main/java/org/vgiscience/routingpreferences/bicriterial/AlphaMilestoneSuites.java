package org.vgiscience.routingpreferences.bicriterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.locationtech.jts.geom.Coordinate;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;

public class AlphaMilestoneSuites implements Comparable<AlphaMilestoneSuites> {

	final String pathId;
	final double alpha;
	final boolean isBound;
	final TreeSet<MilestoneSuite> suites;

	public AlphaMilestoneSuites(String pathId, double alpha, boolean isBound) {
		this.pathId = pathId;
		this.alpha = alpha;
		this.isBound = isBound;
		this.suites = new TreeSet<>();
	}

	public String getPathId() {
		return pathId;
	}

	public double getAlpha() {
		return alpha;
	}

	public boolean isBound() {
		return isBound;
	}

	public ArrayList<MilestoneSuite> getSuites() {
		return new ArrayList<>(suites);
	}

	public void addSuite(MilestoneSuite suite) {
		suites.add(suite);
	}

	public int size() {
		return suites.size();
	}

	public double summedWidth() {
		return suites.stream().mapToDouble(s -> s.width).sum();
	}

	@Override
	public int compareTo(AlphaMilestoneSuites o) {
		if (size() != o.size())
			return Integer.compare(size(), o.size());
		return Double.compare(-summedWidth(), -o.summedWidth());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)//
				.append(pathId)//
				.append(alpha)//
				.append(isBound)//
				.append(suites.size())//
				.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (obj.getClass() != getClass())
			return false;
		AlphaMilestoneSuites rhs = (AlphaMilestoneSuites) obj;
		return new EqualsBuilder()//
				.append(pathId, rhs.pathId)//
				.append(alpha, rhs.alpha)//
				.append(isBound, rhs.isBound)//
				.append(suites, rhs.suites)//
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(37547, 26833)//
				.append(pathId)//
				.append(alpha)//
				.append(isBound)//
				.append(suites)//
				.toHashCode();
	}

	public static final class MilestoneSuite implements Comparable<MilestoneSuite> {

		private final int forwardIdx;
		private final Coordinate forwardMilestone;
		private final int backwardIdx;
		private final Coordinate backwardMilestone;

		private final double width;

		public MilestoneSuite(AlphaMilestoneSuites collection, int forwardIdx, int backwardIdx,
				List<AttributedVertex> vertices) {
			super();
			Objects.requireNonNull(vertices);
			int n = vertices.size();
			if (forwardIdx >= n || backwardIdx >= n || forwardIdx < backwardIdx)
				throw new IllegalArgumentException("indices do not fit");

			this.forwardIdx = forwardIdx;
			this.forwardMilestone = vertices.get(forwardIdx).getLocation();
			this.backwardIdx = backwardIdx;
			this.backwardMilestone = vertices.get(backwardIdx).getLocation();
			this.width = MilestoneSuite.suiteWidth(forwardIdx, backwardIdx, vertices);
		}

		public int getForwardIdx() {
			return forwardIdx;
		}

		public Coordinate getForwardMilestone() {
			return forwardMilestone;
		}

		public int getBackwardIdx() {
			return backwardIdx;
		}

		public Coordinate getBackwardMilestone() {
			return backwardMilestone;
		}

		public double getLength() {
			return width;
		}

		private static double suiteWidth(int fIdx, int bIdx, List<AttributedVertex> vertices) {
			int n = vertices.size();
			if (fIdx >= n || bIdx >= n || fIdx < bIdx)
				throw new IllegalArgumentException("indices do not fit");

			double length = 0;
			Coordinate prev = null;
			for (int i = bIdx; i <= fIdx; ++i) {
				Coordinate curr = vertices.get(i).getLocation();
				if (prev != null)
					length += prev.distance(curr);
				prev = curr;
			}
			return length;
		}

		@Override
		public int compareTo(MilestoneSuite o) {
			if (backwardIdx != o.backwardIdx)
				return Integer.compare(backwardIdx, o.backwardIdx);
			return Double.compare(-width, -o.width);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (obj == this)
				return true;
			if (obj.getClass() != getClass())
				return false;
			MilestoneSuite rhs = (MilestoneSuite) obj;
			return new EqualsBuilder()//
					.append(forwardIdx, rhs.forwardIdx)//
					.append(backwardIdx, rhs.backwardIdx)//
					.append(forwardMilestone, rhs.forwardMilestone)//
					.append(backwardMilestone, rhs.backwardMilestone)//
					.append(width, rhs.width)//
					.isEquals();
		}

		@Override
		public int hashCode() {
			return new HashCodeBuilder(28961, 7457)//
					.append(forwardIdx)//
					.append(backwardIdx)//
					.append(forwardMilestone)//
					.append(backwardMilestone)//
					.append(width)//
					.toHashCode();
		}
	}
}
