package org.vgiscience.routingpreferences.bicriterial;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Function;

import org.vgiscience.orfinder.structures.Interval;

public abstract class LocallyConstantFunction<T> {
	protected Interval range;
	protected TreeMap<Interval, T> intervals;

	public LocallyConstantFunction(Interval range) {
		this.range = range;
		this.intervals = new TreeMap<>();
		this.intervals.put(range, emptyValue());
	}

	public LocallyConstantFunction(Interval range, T initialValue) {
		this.range = range;
		this.intervals = new TreeMap<>();
		this.intervals.put(range, initialValue);
	}

	public T getValueAt(double x) {
		Interval toFloor = new Interval(x, 1);
		return intervals.floorEntry(toFloor).getValue();
	}

	public LocallyConstantFunction<T> getInstance(Interval range) {
		return getInstance(range, emptyValue());
	}

	public abstract LocallyConstantFunction<T> getInstance(Interval range, T value);

	public abstract T emptyValue();

	public abstract T increment(T value, T increment);

	public void set(Interval interval, T value) {
		change(interval, new Function<>() {

			@Override
			public T apply(T t) {
				return value;
			}
		});
	}

	public void increment(Interval interval, T increment) {
		change(interval, new Function<>() {

			@Override
			public T apply(T t) {
				return increment(t, increment);
			}
		});
	}

	public void change(Interval interval, Function<T, T> operation) {
		boolean foundIntersection = false;
		LinkedList<Interval> toRemove = new LinkedList<>();
		HashMap<Interval, T> toAdd = new HashMap<>();
		for (Entry<Interval, T> entry : intervals.entrySet()) {
			Interval curr = entry.getKey();
			T value = entry.getValue();
			if (interval.intersects(curr)) {
				foundIntersection = true;
				// if current interval is identical to searched interval increment it and done!
				if (interval.equals(curr)) {
					entry.setValue(operation.apply(value));
					break;
				}
				// if current interval is contained in searched interval increment it, continue
				if (interval.contains(curr)) {
					entry.setValue(operation.apply(value));
					continue;
				}
				// remove the current interval and add two/three split intervals, increment one
				Interval intersection = interval.intersect(curr);
				Interval lower, upper;
				if (curr.getLowerBound() != intersection.getLowerBound()) {
					assert curr.getLowerBound() < intersection.getLowerBound();
					lower = new Interval(curr.getLowerBound(), curr.lowerBoundInclusive(), intersection.getLowerBound(),
							!intersection.lowerBoundInclusive());
					toAdd.put(lower, value);
				}
				if (curr.getUpperBound() != intersection.getUpperBound()) {
					assert interval.getUpperBound() < curr.getUpperBound();
					upper = new Interval(intersection.getUpperBound(), !intersection.upperBoundInclusive(),
							curr.getUpperBound(), curr.upperBoundInclusive());
					toAdd.put(upper, value);
				}
				toAdd.put(intersection, operation.apply(value));
				toRemove.add(curr);
			}
			// stop when the intersection has been found already but the intervals stopped
			// intersecting
			else if (foundIntersection)
				break;
		}

		for (Interval rem : toRemove)
			intervals.remove(rem);
		intervals.putAll(toAdd);

		mergeEqualIntervals();
	}

	private void mergeEqualIntervals() {
		Interval prev = null;
		T activeVal = null;
		LinkedList<Interval> activeIntervals = null;
		HashMap<LinkedList<Interval>, T> toCombine = new HashMap<>();
		for (Entry<Interval, T> entry : intervals.entrySet()) {
			Interval curr = entry.getKey();
			T currVal = entry.getValue();
			if (prev != null) {
				assert !prev.intersects(curr);
				assert prev.getUpperBound() == curr.getLowerBound();

				if (currVal == activeVal) {
					activeIntervals.add(curr);
				} else {
					if (activeIntervals.size() > 1)
						toCombine.put(activeIntervals, activeVal);

					activeVal = currVal;
					activeIntervals = new LinkedList<>();
					activeIntervals.add(curr);
				}
			} else {
				activeVal = currVal;
				activeIntervals = new LinkedList<>();
				activeIntervals.add(curr);
			}
			prev = curr;
		}

		if (activeIntervals.size() > 1)
			toCombine.put(activeIntervals, activeVal);

		for (Entry<LinkedList<Interval>, T> entry : toCombine.entrySet()) {
			LinkedList<Interval> toMerge = entry.getKey();
			for (Interval interval : toMerge)
				intervals.remove(interval);
			Interval newInterval = new Interval(//
					toMerge.getFirst().getLowerBound(), //
					toMerge.getFirst().lowerBoundInclusive(), //
					toMerge.getLast().getUpperBound(), //
					toMerge.getLast().upperBoundInclusive()//
			);
			intervals.put(newInterval, entry.getValue());
		}
	}

	public LocallyConstantFunction<T> add(LocallyConstantFunction<T> function2) {
		LocallyConstantFunction<T> res = getInstance(this.range);

		Entry<Interval, T> active1 = intervals.firstEntry();
		Entry<Interval, T> active2 = function2.intervals.firstEntry();

		while (active1 != null && active2 != null) {
			Interval intersection = active1.getKey().intersect(active2.getKey());
			T val = increment(active1.getValue(), active2.getValue());
			if (intersection != null)
				res.set(intersection, val);

			if (active1.getKey().getUpperBound() < active2.getKey().getUpperBound())
				active1 = intervals.higherEntry(active1.getKey());
			else
				active2 = function2.intervals.higherEntry(active2.getKey());
		}

		res.mergeEqualIntervals();
		return res;
	}

	public static <T> LocallyConstantFunction<T> add(LocallyConstantFunction<T> function1,
			LocallyConstantFunction<T> function2) {
		return function1.add(function2);
	}
}
