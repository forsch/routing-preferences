package org.vgiscience.routingpreferences.bicriterial;

import java.util.List;
import java.util.stream.Collectors;

import org.vgiscience.orfinder.structures.Interval;

public class LocallyConstantIntegerFunction extends LocallyConstantFunction<Integer> {

	public LocallyConstantIntegerFunction(Interval range) {
		super(range);
	}

	public LocallyConstantIntegerFunction(Interval range, Integer value) {
		super(range, value);
	}

	@Override
	public Integer increment(Integer value, Integer increment) {
		return value.intValue() + increment.intValue();
	}

	@Override
	public Integer emptyValue() {
		return 0;
	}

	@Override
	public LocallyConstantFunction<Integer> getInstance(Interval range, Integer value) {
		return new LocallyConstantIntegerFunction(range, value);
	}

	@Override
	public LocallyConstantIntegerFunction add(LocallyConstantFunction<Integer> function2) {
		return (LocallyConstantIntegerFunction) super.add(function2);
	}

	public Integer minValue() {
		return intervals.values().stream().min(Integer::compare).get();
	}

	public List<Interval> minValueIntervals() {
		int minValue = minValue();
		return intervals.entrySet().stream()//
				.filter(entry -> entry.getValue() == minValue)//
				.map(entry -> entry.getKey())//
				.collect(Collectors.toList());
	}
}