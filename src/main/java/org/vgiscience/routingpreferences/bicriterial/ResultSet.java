package org.vgiscience.routingpreferences.bicriterial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.Pair;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;

public class ResultSet implements Comparable<ResultSet> {
	// private static final Logger logger =
	// Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private String pathId;
	private ArrayList<AttributedVertex> vert;
	private TreeMap<Double, AlphaMilestoneSuites> suites;
	private ArrayList<Pair<Integer, Interval>> multiAlphaDecom;
	private Interval searchInterval;
	private int primaryWeightIndex, secondaryWeightIndex;

	public ResultSet(String id, ArrayList<AttributedVertex> vert, TreeMap<Double, AlphaMilestoneSuites> suites,
			ArrayList<Pair<Integer, Interval>> multiAlphaDecom) {
		this(id, vert, suites, multiAlphaDecom, new Interval());
	}

	public ResultSet(String pathId, ArrayList<AttributedVertex> vert, TreeMap<Double, AlphaMilestoneSuites> suites,
			ArrayList<Pair<Integer, Interval>> multiAlphaDecom, Interval searchInterval) {
		this.pathId = pathId;
		this.vert = vert;
		this.multiAlphaDecom = multiAlphaDecom;
		this.searchInterval = searchInterval;
		this.suites = Objects.requireNonNull(suites);
	}

	public TreeMap<Double, AlphaMilestoneSuites> getMilestoneSuites() {
		return suites;
	}

	public ArrayList<AttributedVertex> getVertices() {
		return vert;
	}

	public AlphaMilestoneSuites getSuiteAt(double alpha) {
		AlphaMilestoneSuites prev = null;
		for (AlphaMilestoneSuites curr : suites.values()) {
			if (curr.getAlpha() == alpha)
				return curr;
			if (curr.getAlpha() > alpha) {
				if (prev == null)
					return curr;
				if (curr.isBound())
					return prev;
				return curr;
			}
			prev = curr;
		}
		return suites.lastEntry().getValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ResultSet))
			return false;

		ResultSet other = (ResultSet) obj;

		if (!pathId.equals(other.pathId))
			return false;
		if (!searchInterval.equals(other.searchInterval))
			return false;
		if (primaryWeightIndex != other.primaryWeightIndex)
			return false;
		if (secondaryWeightIndex != other.secondaryWeightIndex)
			return false;

		for (Entry<Double, AlphaMilestoneSuites> entry : suites.entrySet()) {
			if (!other.suites.containsKey(entry.getKey()))
				return false;
			if (!entry.getValue().equals(other.suites.get(entry.getKey())))
				return false;
		}

		return true;
	}

	public boolean equalsTopo(ResultSet other) {
		Interval commonInterval = searchInterval.intersect(other.searchInterval);

		LinkedList<AlphaMilestoneSuites> decomOwn = new LinkedList<>(suites.values());
		LinkedList<AlphaMilestoneSuites> decomOther = new LinkedList<>(other.suites.values());

		AlphaMilestoneSuites ownD = decomOwn.poll();
		while (ownD.alpha <= commonInterval.getLowerBound() || !ownD.isBound)
			ownD = decomOwn.poll();

		AlphaMilestoneSuites otherD = decomOther.poll();
		while (otherD.alpha <= commonInterval.getLowerBound() || !otherD.isBound)
			otherD = decomOther.poll();

		while (ownD.alpha < commonInterval.getUpperBound() && otherD.alpha < commonInterval.getUpperBound()) {
			if (!ownD.isBound && !ownD.suites.equals(otherD.suites))
				return false;
			else if (ownD.isBound && !ownD.equals(otherD))
				return false;
			ownD = decomOwn.poll();
			otherD = decomOther.poll();
		}
		return true;
	}

	public AlphaMilestoneSuites getBestSuite() {
		LinkedList<AlphaMilestoneSuites> sorted = new LinkedList<>(suites.values());
		Collections.sort(sorted);
		return sorted.getFirst();
	}

	@Override
	public String toString() {
		return "ResultSet [p=" + primaryWeightIndex + ", s=" + secondaryWeightIndex + ", bestDecom="
				+ getBestSuite() + "]";
	}

	@Override
	public int compareTo(ResultSet o) {
		return getMilestoneSuites().firstEntry().getValue().compareTo(o.getMilestoneSuites().firstEntry().getValue());
	}

	public int getPrimaryWeightIndex() {
		return primaryWeightIndex;
	}

	public int getSecondaryWeightIndex() {
		return secondaryWeightIndex;
	}

	public void setWeightIndices(int primaryWeightIndex, int secondaryWeightIndex) {
		this.primaryWeightIndex = primaryWeightIndex;
		this.secondaryWeightIndex = secondaryWeightIndex;
	}

	public LocallyConstantIntegerFunction toFunction() {
		LocallyConstantIntegerFunction function = new LocallyConstantIntegerFunction(searchInterval);

		AlphaMilestoneSuites prevBound = null;
		AlphaMilestoneSuites inter = null;
		for (AlphaMilestoneSuites currBound : suites.values()) {
			if (prevBound == null) {
				assert currBound.isBound;
				prevBound = currBound;
				continue;
			}
			if (!currBound.isBound) {
				inter = currBound;
				continue;
			}

			boolean aInclusive = prevBound.size() == inter.size();
			boolean bInclusive = inter.size() == currBound.size();
			Interval interval = new Interval(prevBound.alpha, aInclusive, currBound.alpha, bInclusive);

			int size = inter.size();
			function.set(interval, size);
			if (!aInclusive)
				function.set(new Interval(prevBound.alpha, prevBound.alpha), prevBound.size());
			if (!bInclusive)
				function.set(new Interval(currBound.alpha, currBound.alpha), currBound.size());
			prevBound = currBound;
		}

		return function;
	}

	public String getPathId() {
		return pathId;
	}

	public ArrayList<Pair<Integer, Interval>> getMultiAlphaDecom() {
		return multiAlphaDecom;
	}
}
