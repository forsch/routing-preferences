package org.vgiscience.routingpreferences.io;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPoint;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.vgiscience.routingpreferences.bicriterial.AlphaMilestoneSuites;
import org.vgiscience.routingpreferences.bicriterial.AlphaMilestoneSuites.MilestoneSuite;
import org.vgiscience.routingpreferences.bicriterial.ResultSet;

import de.geoinfobonn.io.format.shp.ShapefileWriter;

public class ResultSetToShp extends ShapefileWriter {

	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	public static void exportToSHP(File outdir, String suffix, List<ResultSet> results) throws IOException {
		if (!outdir.exists())
			outdir.mkdirs();
		if (!outdir.isDirectory())
			throw new IllegalArgumentException("outdir is not a directory! outdir=" + outdir.getAbsolutePath());
		if (results.isEmpty()) {
			logger.warning("Empty result list, skipping export of results.");
			return;
		}

		CoordinateReferenceSystem crs = null;
		try {
			crs = CRS.decode("EPSG:3044");
		} catch (NoSuchAuthorityCodeException e) {
			e.printStackTrace();
		} catch (FactoryException e) {
			e.printStackTrace();
		}

		exportTracks(outdir, suffix, results, Optional.ofNullable(crs));
		exportMilestoneSuites(outdir, suffix, results, Optional.ofNullable(crs));
	}

	public static void exportMilestoneSuites(File outdir, String suffix, List<ResultSet> results,
			Optional<CoordinateReferenceSystem> crs) throws IOException {
		if (results.isEmpty()) {
			logger.warning("Empty result list, skipping export of milestone suites.");
			return;
		}

		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("suites");
		builder.setCRS(crs.orElse(DefaultGeographicCRS.WGS84)); // <- Coordinate reference system

		// add attributes in order
		builder.add("the_geom", MultiPoint.class);
		builder.add("pathId", String.class);
		builder.add("alpha", Double.class);
		builder.add("idx", Integer.class);
		builder.add("fIdx", Integer.class);
		builder.add("bIdx", Integer.class);
		builder.add("length", Double.class);

		// build the type
		final SimpleFeatureType SUITES = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(SUITES);

		for (ResultSet result : results) {
			AlphaMilestoneSuites best = result.getBestSuite();
			AlphaMilestoneSuites shortest = result.getSuiteAt(0.5);
			List<AlphaMilestoneSuites> suitesToExport = new LinkedList<>();
			suitesToExport.add(best);
			suitesToExport.add(shortest);
			int i = 0;
			for (AlphaMilestoneSuites suites : suitesToExport) {
				for (MilestoneSuite suite : suites.getSuites()) {
					Coordinate[] c = new Coordinate[2];
					c[0] = suite.getForwardMilestone();
					c[1] = suite.getBackwardMilestone();
					MultiPoint mp = gf.createMultiPointFromCoords(c);

					featureBuilder.add(mp);
					featureBuilder.add(suites.getPathId());
					featureBuilder.add(suites.getAlpha());
					featureBuilder.add(i);
					featureBuilder.add(suite.getForwardIdx());
					featureBuilder.add(suite.getForwardIdx());
					featureBuilder.add(suite.getLength());

					SimpleFeature feature = featureBuilder.buildFeature(null);
					collection.add(feature);
				}
				++i;
			}
		}

		ShapefileWriter.writeToShapefile(outdir, "suites_" + suffix, collection);
	}

	public static void exportTracks(File outdir, String suffix, List<ResultSet> results,
			Optional<CoordinateReferenceSystem> crs) throws IOException {
		if (results.isEmpty()) {
			logger.warning("Empty result list, skipping export of tracks.");
			return;
		}

		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("paths");
		builder.setCRS(crs.orElse(DefaultGeographicCRS.WGS84)); // <- Coordinate reference system

		// add attributes in order
		builder.add("the_geom", LineString.class);
		builder.add("pathId", String.class);
		builder.add("primIdx", Integer.class);
		builder.add("secIdx", Integer.class);

		// build the type
		final SimpleFeatureType PATHS = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(PATHS);

		for (ResultSet result : results) {
			Coordinate[] c = result.getVertices().stream()//
					.map(v -> v.getLocation())//
					.toArray(Coordinate[]::new);

			featureBuilder.add(gf.createLineString(c));
			featureBuilder.add(result.getPathId());
			featureBuilder.add(result.getPrimaryWeightIndex());
			featureBuilder.add(result.getSecondaryWeightIndex());

			SimpleFeature feature = featureBuilder.buildFeature(null);
			collection.add(feature);
		}

		ShapefileWriter.writeToShapefile(outdir, "paths_" + suffix, collection);
	}
}
