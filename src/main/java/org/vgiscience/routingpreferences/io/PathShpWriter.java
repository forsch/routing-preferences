package org.vgiscience.routingpreferences.io;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultEdge;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.DoubleCostFunction;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedEdge;
import org.vgiscience.routingpreferences.graph.simplified.WeightedSimplifiedRoadGraph.SimplifiedDoubleCostFunction;

import de.geoinfobonn.io.format.shp.ShapefileWriter;

public class PathShpWriter extends ShapefileWriter {

	public static <E extends DefaultEdge> void export(File outdir, Optional<String> suffix,
			GraphPath<AttributedVertex, E> path, Optional<DoubleCostFunction> basicCostFunction,
			Optional<SimplifiedDoubleCostFunction> simplifiedCostFunction, Optional<CoordinateReferenceSystem> crs)
			throws IOException {
		Optional<String> vertexSuffix = Optional.of(suffix.orElse("") + "_vertices");
		exportVertices(outdir, vertexSuffix, path, crs);

		Optional<String> edgeSuffix = Optional.of(suffix.orElse("") + "_edges");
		exportEdges(outdir, edgeSuffix, path, basicCostFunction, simplifiedCostFunction, crs);
	}

	private static <E extends DefaultEdge> void exportVertices(File outdir, Optional<String> suffix,
			GraphPath<AttributedVertex, E> path, Optional<CoordinateReferenceSystem> crs) throws IOException {
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("vertices");
		builder.setCRS(crs.orElse(DefaultGeographicCRS.WGS84)); // <- Coordinate reference system

		// add attributes in order
		builder.add("the_geom", Point.class);
		builder.add("seq", Integer.class);

		// build the type
		final SimpleFeatureType VERTICES = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(VERTICES);

		int i = 0;
		for (AttributedVertex v : path.getVertexList()) {
			Coordinate c = v.getLocation();

			featureBuilder.add(gf.createPoint(c));
			featureBuilder.add(i++);

			SimpleFeature feature = featureBuilder.buildFeature(null);
			collection.add(feature);
		}

		ShapefileWriter.writeToShapefile(outdir, "path" + suffix.orElse(""), collection);
	}

	private static <E extends DefaultEdge> void exportEdges(File outdir, Optional<String> suffix,
			GraphPath<AttributedVertex, E> path, Optional<DoubleCostFunction> basicCostFunction,
			Optional<SimplifiedDoubleCostFunction> simplifiedCostFunction, Optional<CoordinateReferenceSystem> crs)
			throws IOException {
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("edges");
		builder.setCRS(crs.orElse(DefaultGeographicCRS.WGS84)); // <- Coordinate reference system

		// add attributes in order
		builder.add("the_geom", LineString.class);
		builder.add("seq", Integer.class);
		builder.length(1).add("upwards", String.class);
		builder.add("weight", Double.class);
		builder.add("attributes", String.class);

		// build the type
		final SimpleFeatureType EDGES = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(EDGES);

		List<AttributedVertex> vertices = path.getVertexList();
		List<E> edges = path.getEdgeList();
		for (int i = 0; i < path.getLength(); ++i) {
			AttributedVertex s = vertices.get(i);
			AttributedVertex t = vertices.get(i + 1);
			Coordinate c[] = { s.getLocation(), t.getLocation() };

			String upwards = s.getLocation().compareTo(t.getLocation()) < 0 ? "T" : "F";
			E edge = edges.get(i);
			double weight = 0;
			String attributes = "";
			if (edge instanceof SimplifiedEdge) {
				weight = simplifiedCostFunction.map(cf -> cf.apply((SimplifiedEdge) edge)).orElse(-1.);
				attributes = ((SimplifiedEdge) edge).getSimplifiedEdges().stream()
						.map(e -> e.getAttributes().toString()).collect(Collectors.joining(","));
			} else if (edge instanceof AttributedEdge) {
				weight = basicCostFunction.map(cf -> cf.apply((AttributedEdge) edge)).orElse(-1.);
				attributes = ((AttributedEdge) edge).getAttributes().toString();
			}

			featureBuilder.add(gf.createLineString(c));
			featureBuilder.add(i);
			featureBuilder.add(upwards);
			featureBuilder.add(weight);
			featureBuilder.add(attributes);

			SimpleFeature feature = featureBuilder.buildFeature(null);
			collection.add(feature);
		}

		ShapefileWriter.writeToShapefile(outdir, "path" + suffix.orElse(""), collection);
	}
}
