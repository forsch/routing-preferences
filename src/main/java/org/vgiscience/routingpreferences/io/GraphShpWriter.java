package org.vgiscience.routingpreferences.io;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.logging.Logger;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedEdge;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedRoadGraph;

import de.geoinfobonn.io.format.shp.ShapefileWriter;

public class GraphShpWriter extends ShapefileWriter {

	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	public static void exportToShapefile(SimplifiedRoadGraph sg, File outdir, Optional<String> prefix,
			Optional<String> suffix, Optional<CoordinateReferenceSystem> crs) {
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("paths");
		crs.ifPresent(builder::setCRS);
		// builder.setCRS(crs.orElse(DefaultGeographicCRS.WGS84)); // <- Coordinate
		// reference system

		// add attributes in order
		builder.add("the_geom", LineString.class);
		builder.length(1).add("upwards", String.class);
		builder.add("skippedE", Integer.class);
		builder.add("edges", String.class);

		// build the type
		final SimpleFeatureType PATHS = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(PATHS);

		for (SimplifiedEdge edge : sg.edgeSet()) {
			AttributedVertex s = sg.getEdgeSource(edge);
			AttributedVertex t = sg.getEdgeTarget(edge);
			Coordinate[] c = new Coordinate[] { s.getLocation(), t.getLocation() };
			String upwards = s.getLocation().compareTo(t.getLocation()) < 0 ? "T" : "F";

			featureBuilder.add(gf.createLineString(c));
			featureBuilder.add(upwards);
			featureBuilder.add(edge.getSimplifiedEdges().size());
			String edges = "";
			for (AttributedEdge e : edge.getSimplifiedEdges()) {
				double factor = 2;
				if (e.hasAttribute("factor"))
					factor = (Double) e.getAttribute("factor").get().getValue();
				edges += "(" + e.getAttributeValue("length") + "," + factor + "), ";
			}
			featureBuilder.add(edges);

			SimpleFeature feature = featureBuilder.buildFeature(null);
			collection.add(feature);
		}

		try {
			ShapefileWriter.writeToShapefile(outdir, prefix.orElse("") + "simplifiedGraph" + suffix.orElse(""),
					collection);
		} catch (IOException e) {
			logger.severe("Error while exporting simplified road graph: " + e.getMessage());
		}
	}

	public static void exportToShapefile(RoadGraph rg, File outdir, Optional<String> prefix, Optional<String> suffix,
			Optional<CoordinateReferenceSystem> crs) {
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName("graph");
		crs.ifPresent(builder::setCRS);

		// add attributes in order
		builder.add("the_geom", LineString.class);
		builder.length(1).add("upwards", String.class);
		builder.add("length", Double.class);
		builder.add("factor", Double.class);

		// build the type
		final SimpleFeatureType GRAPH = builder.buildFeatureType();

		DefaultFeatureCollection collection = new DefaultFeatureCollection();
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory(null);
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(GRAPH);

		for (AttributedEdge edge : rg.edgeSet()) {
			AttributedVertex s = rg.getEdgeSource(edge);
			AttributedVertex t = rg.getEdgeTarget(edge);
			Coordinate[] c = new Coordinate[] { s.getLocation(), t.getLocation() };
			String upwards = s.getLocation().compareTo(t.getLocation()) < 0 ? "T" : "F";

			featureBuilder.add(gf.createLineString(c));
			featureBuilder.add(upwards);
			edge.getAttribute("length").ifPresent(a -> {
				featureBuilder.add(a.getValue());
			});
			edge.getAttribute("factor").ifPresent(a -> {
				featureBuilder.add(a.getValue());
			});

			SimpleFeature feature = featureBuilder.buildFeature(null);
			collection.add(feature);
		}

		try {
			ShapefileWriter.writeToShapefile(outdir, prefix.orElse("") + "roadGraph" + suffix.orElse(""), collection);
		} catch (IOException e) {
			logger.severe("Error while exporting road graph: " + e.getMessage());
		}
	}
}
