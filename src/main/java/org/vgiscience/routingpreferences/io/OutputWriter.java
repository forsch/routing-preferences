package org.vgiscience.routingpreferences.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.logging.Logger;

import org.vgiscience.routingpreferences.bicriterial.AlphaMilestoneSuites;
import org.vgiscience.routingpreferences.bicriterial.AlphaMilestoneSuites.MilestoneSuite;
import org.vgiscience.routingpreferences.bicriterial.IntervalMatrixArray;
import org.vgiscience.routingpreferences.bicriterial.ResultSet;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.LongCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.path.RoadPath;
import org.vgiscience.routingpreferences.path.RoadPathLineProvider;

public class OutputWriter<T, S> {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private RoadGraph g;
	private RoadPath path;
	private String currentName;

	private T currentTrackId;
	private S currentSubtrackId;
	private double geomLength = 0;

	private boolean printCounts = true;

	private File outputStatsFile;
	private File outputIntervalMatrixDir;
	private File outputDecomDir;
	private File outputPathDir;

	public OutputWriter(RoadGraph g) {
		this.g = g;
	}

	public void setOutputDirectory(File directory) {
		if (directory.mkdirs())
			logger.info("Created output directory " + directory);
		else
			logger.finer("Using preexisting output directory " + directory);

		this.outputStatsFile = new File(directory, "algorithm_stats.csv");

		this.outputIntervalMatrixDir = new File(directory, "intervals/");
		if (outputIntervalMatrixDir.mkdirs())
			logger.info("Created interval directory " + outputIntervalMatrixDir);
		else
			logger.finer("Using preexisting interval directory " + outputIntervalMatrixDir);

		this.outputDecomDir = new File(directory, "decom/");
		if (outputDecomDir.mkdirs())
			logger.info("Created decomposition directory " + outputDecomDir);
		else
			logger.finer("Using preexisting decomposition directory " + outputDecomDir);

		this.outputPathDir = new File(directory, "paths/");
		if (outputPathDir.mkdirs())
			logger.info("Created path directory " + outputPathDir);
		else
			logger.finer("Using preexisting path directory " + outputPathDir);
	}

	public void setCurrentData(T trackId, S subtrackId, double geomLength, String name) {
		this.currentTrackId = trackId;
		this.currentSubtrackId = subtrackId;
		this.geomLength = geomLength;
		this.currentName = name;
	}

	public void setCurrentPath(RoadPath path) {
		this.path = path;
	}

	public String currentFilename() {
		String name = currentName != null ? currentName : "";
		return name + "_" + currentTrackId + "_" + currentSubtrackId;
	}

	private String generateHeader() {
		StringBuffer sb = new StringBuffer();
		sb.append("name;");
		sb.append("track_id;");
		sb.append("subtrack_id;");
		sb.append("geomLength;");
		sb.append("size;");
		sb.append("prim;");
		sb.append("sec;");
		sb.append("primO0;");
		sb.append("secO0;");
		sb.append("primO1;");
		sb.append("secO1;");
		sb.append("t_algo;");
		sb.append("n_dijk;");
		sb.append("t_dijk;");
		sb.append("opt_alpha;");
		sb.append("opt_ms;");
		sb.append("opt_ms_width;");
		sb.append("shortest_ms;");
		sb.append("shortest_ms_width;");
		sb.append("multi_ms;");
		sb.append("message;");

		if (printCounts) {
			sb.append("countNone;");
			sb.append("countIsOptimal;");
			sb.append("countPointOnLE;");
			sb.append("countBounds;");
		}

		return sb.toString();
	}

	public void writeStatsHeader() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputStatsFile))) {
			bw.write(generateHeader());
			bw.newLine();
		} catch (IOException e) {
			logger.severe("Error during `writeStatsHeader` occured. " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void writeIMA() throws IOException {
		IntervalMatrixArray ima = path.getIntervalMatrixArray();
		ima.export(new File(outputIntervalMatrixDir, "ima_" + currentFilename() + ".csv").getAbsolutePath());
	}

	public void writeDecomposition(ResultSet rs) throws IOException {
		{
			BufferedWriter bw_dec = new BufferedWriter(
					new FileWriter(new File(outputDecomDir, "dec_" + currentFilename() + ".csv")));
			BufferedWriter bw_inter = new BufferedWriter(
					new FileWriter(new File(outputDecomDir, "dec_" + currentFilename() + "_inter.csv")));
			BufferedWriter curr_bw;

			ArrayList<AlphaMilestoneSuites> suites = new ArrayList<>(rs.getMilestoneSuites().values());
			Collections.sort(suites);
			for (AlphaMilestoneSuites ad : suites) {
				if (ad.isBound())
					curr_bw = bw_dec;
				else
					curr_bw = bw_inter;

				curr_bw.write((ad.getSuites() == null ? "0;" : ad.getSuites().size() + ";") + ad.getAlpha() + ";");
				for (MilestoneSuite s : ad.getSuites()) {
					curr_bw.write(s.getBackwardIdx() + ",");
				}
				curr_bw.write(";");
				for (MilestoneSuite s : ad.getSuites()) {
					curr_bw.write(s.getForwardIdx() + ",");
				}
				curr_bw.write(";");
				curr_bw.write("" + ad.summedWidth());
				curr_bw.newLine();
			}
			bw_dec.close();
			bw_inter.close();
		}
	}

	public void writeStats(String message, LongCostFunction primary, LongCostFunction secondary, ResultSet rs) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputStatsFile, true));

			bw.write(generateOutput(message, primary, secondary, rs));

			bw.newLine();
			bw.close();
		} catch (Exception e) {
			logger.severe("Error during `writeStats` occured. " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void writeStats(LongCostFunction primary, LongCostFunction secondary, ResultSet rs) {
		writeStats("", primary, secondary, rs);
	}

	public void writeFailMessage(String message) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputStatsFile, true));

			bw.write(failMessage(message));

			bw.newLine();
			bw.close();
		} catch (Exception e) {
			logger.severe("Error during `writeStats` occured. " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void writeFailMessage(Exception e) {
		writeFailMessage(e.toString());
	}

	private String failMessage(String message) {
		StringBuffer sb = new StringBuffer();
		sb.append(currentName + ";");
		sb.append(currentTrackId + ";");
		sb.append(currentSubtrackId + ";");
		sb.append(String.format("%.2f;", geomLength));
		sb.append(";;;;;;;;;;;;;;;;");
		sb.append(message);
		sb.append(";;;;;");
		return sb.toString();
	}

	private String generateOutput(String message, LongCostFunction primary, LongCostFunction secondary, ResultSet rs) {
		StringBuffer sb = new StringBuffer();
		sb.append(currentName + ";");
		sb.append(currentTrackId + ";");
		sb.append(currentSubtrackId + ";");
		sb.append(String.format("%.2f;", geomLength));
		if (path != null) {
			sb.append(path.getLength() + ";");
			sb.append(path.evaluate(primary) + ";");
			sb.append(path.evaluate(secondary) + ";");

			AttributedVertex s = path.getStartVertex();
			AttributedVertex t = path.getEndVertex();

			RoadPathLineProvider lp = new RoadPathLineProvider(g, path, primary, secondary);
			RoadPath o0 = lp.findShortestPath(s, t, 0, Optional.empty());
			RoadPath o1 = lp.findShortestPath(s, t, 1, Optional.empty());

			sb.append(o0.evaluate(primary) + ";");
			sb.append(o0.evaluate(secondary) + ";");
			sb.append(o1.evaluate(primary) + ";");
			sb.append(o1.evaluate(secondary) + ";");

			IntervalMatrixArray ima = path.getIntervalMatrixArray();
			sb.append(ima.getAlgoTime() + ";");
			sb.append(ima.getNumDijkstraCalls() + ";");
			sb.append(ima.getDijkstraTime() + ";");
			sb.append(rs.getBestSuite().getAlpha() + ";");
			sb.append(rs.getBestSuite().size() + ";");
			sb.append(rs.getBestSuite().summedWidth() + ";");
			sb.append(rs.getSuiteAt(0.5).size() + ";");
			sb.append(rs.getSuiteAt(0.5).summedWidth() + ";");
			sb.append(rs.getMultiAlphaDecom().size() - 1 + ";");
			sb.append(";"); // skip message

			try {
				o0.exportToCSV(new File(outputPathDir, currentFilename() + "_o0.csv"), primary, secondary);
				o1.exportToCSV(new File(outputPathDir, currentFilename() + "_o1.csv"), primary, secondary);
				path.exportToCSV(new File(outputPathDir, currentFilename() + "_p.csv"), primary, secondary);
			} catch (Exception e) {
				logger.severe("Error during `generateOutput` occured while exporting paths to csv. currentName="
						+ currentName + ", currentTrackId=" + currentTrackId + ", currentSubtrackId="
						+ currentSubtrackId + e.getMessage());
				e.printStackTrace();
			}

			if (printCounts) {
				sb.append(ima.getCountNone() + ";");
				sb.append(ima.getCountIsOptimal() + ";");
				sb.append(ima.getCountPointOnLE() + ";");
				sb.append(ima.getCountBounds() + ";");
			}
		} else {
			sb.append(";;;;;;;;;;");
			sb.append(message + ";");
			sb.append(";;;;;");
		}

		return sb.toString();
	}
}
