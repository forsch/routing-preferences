package org.vgiscience.routingpreferences.datatypes;

public class DoubleAttribute extends Attribute<Double> {

	public DoubleAttribute(String id, double value) {
		super(id, value);
	}

	@Override
	public String toString() {
		return "[" + id + "=" + String.format("%.2f", value) + "]";
	}
}
