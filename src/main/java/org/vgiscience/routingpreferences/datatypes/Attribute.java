package org.vgiscience.routingpreferences.datatypes;

import java.util.Objects;

public class Attribute<T> {

	protected String id;
	protected T value;

	public Attribute(String id, T value) {
		Objects.requireNonNull(id);
		Objects.requireNonNull(value);
		if (id.isBlank())
			throw new IllegalArgumentException("id cannot be empty!");
		this.id = id;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public T getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "[" + id + "=" + value + "]";
	}
}
