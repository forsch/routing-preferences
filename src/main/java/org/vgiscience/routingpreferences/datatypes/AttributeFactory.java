package org.vgiscience.routingpreferences.datatypes;

import de.geoinfobonn.io.model.graph.FeatureEdge;
import de.geoinfobonn.io.model.graph.FeatureVertex;

public interface AttributeFactory<T> {
	Attribute<T> getAttribute(FeatureEdge edge, FeatureVertex source, FeatureVertex target);

	public static final AttributeFactory<Double> ASCENT = new AttributeFactory<>() {

		public static final String FEATURE_NAME = "height";
		public static final String ATTRIBUTE_NAME = "ascent";

		@Override
		public Attribute<Double> getAttribute(FeatureEdge edge, FeatureVertex source, FeatureVertex target) {
			if (!source.hasFeature() || !target.hasFeature())
				throw new RuntimeException("source or target does not have a feature!");
			if (source.getFeature().getAttribute(FEATURE_NAME) == null
					|| target.getFeature().getAttribute(FEATURE_NAME) == null)
				throw new RuntimeException("source or target does not have a " + FEATURE_NAME + " attribute!");

			double ascent = Math.abs((double) target.getFeature().getAttribute(FEATURE_NAME)
					- (double) source.getFeature().getAttribute(FEATURE_NAME));

			return new DoubleAttribute(ATTRIBUTE_NAME, ascent);
		}
	};

	public static final AttributeFactory<Double> LENGTH = new AttributeFactory<>() {

		public static final String ATTRIBUTE_NAME = "length";

		@Override
		public Attribute<Double> getAttribute(FeatureEdge edge, FeatureVertex source, FeatureVertex target) {
			return new DoubleAttribute(ATTRIBUTE_NAME, source.getLocation().distance(target.getLocation()));
		}
	};

	public static final AttributeFactory<String> SURFACE = new AttributeFactory<>() {

		public static final String FEATURE_NAME = "surface";
		public static final String ATTRIBUTE_NAME = "surface";

		@Override
		public Attribute<String> getAttribute(FeatureEdge edge, FeatureVertex source, FeatureVertex target) {
			if (!edge.hasFeature())
				throw new RuntimeException("edge does not have a feature!");
			if (edge.getFeature().getAttribute(FEATURE_NAME) == null)
				throw new RuntimeException("source or target does not have a " + FEATURE_NAME + " attribute!");
			return new Attribute<>(ATTRIBUTE_NAME, (String) edge.getFeature().getAttribute(FEATURE_NAME));
		}
	};

	public static final AttributeFactory<Boolean> CYCLING = new AttributeFactory<>() {

		public static final String FEATURE_NAME = "partOfCN";
		public static final String ATTRIBUTE_NAME = "cycling";

		@Override
		public Attribute<Boolean> getAttribute(FeatureEdge edge, FeatureVertex source, FeatureVertex target) {
			if (!edge.hasFeature())
				throw new RuntimeException("edge does not have a feature!");
			if (edge.getFeature().getAttribute(FEATURE_NAME) == null)
				throw new RuntimeException("source or target does not have a " + FEATURE_NAME + " attribute!");
			return new Attribute<>(ATTRIBUTE_NAME, edge.getFeature().getAttribute(FEATURE_NAME).equals("T"));
		}
	};
}