package org.vgiscience.routingpreferences.datatypes;

import java.lang.invoke.MethodHandles;
import java.util.LinkedHashMap;
import java.util.logging.Logger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.locationtech.jts.geom.Coordinate;

public class AttributedVertex {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private Coordinate location;
	private int layer;
	private LinkedHashMap<String, Attribute<?>> attributes;

	public AttributedVertex(Coordinate location, int layer) {
		this.location = location;
		this.layer = layer;
		this.attributes = new LinkedHashMap<>();
	}

	public AttributedVertex(Coordinate location) {
		this(location, 0);
	}

	public boolean addAttribute(Attribute<?> attribute) {
		if (attributes.containsKey(attribute.getId())) {
			logger.warning("Did not add attribute. Attribute already present");
			return false;
		}
		attributes.put(attribute.getId(), attribute);
		return true;
	}

	public Coordinate getLocation() {
		return location;
	}

	@Override
	public String toString() {
		return String.format("AttributedVertex [(%.3f, %.3f), layer=%d, attributes=%s]", location.getX(),
				location.getY(), layer, attributes.toString());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (obj.getClass() != getClass())
			return false;

		AttributedVertex rhs = (AttributedVertex) obj;
		return new EqualsBuilder()//
				.append(location, rhs.location)//
				.append(layer, rhs.layer)//
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(2749, 3433)//
				.append(location)//
				.append(layer)//
				.toHashCode();
	}
}
