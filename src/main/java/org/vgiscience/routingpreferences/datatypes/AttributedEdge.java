package org.vgiscience.routingpreferences.datatypes;

import java.lang.invoke.MethodHandles;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Logger;

import org.jgrapht.graph.DefaultEdge;

public class AttributedEdge extends DefaultEdge {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final long serialVersionUID = 813812690872285880L;

	protected LinkedHashMap<String, Attribute<?>> attributes;

	public AttributedEdge() {
		this.attributes = new LinkedHashMap<>();
	}

	public boolean addAttribute(Attribute<?> attribute) {
		if (attributes.containsKey(attribute.getId())) {
			logger.warning("Did not add attribute. Attribute already present");
			return false;
		}
		attributes.put(attribute.getId(), attribute);
		return true;
	}

	public boolean hasAttribute(String id) {
		return attributes.containsKey(id);
	}

	public Optional<Attribute<?>> getAttribute(String id) {
		return Optional.ofNullable(attributes.get(id));
	}

	public Object getAttributeValue(String id) {
		if (!attributes.containsKey(id))
			throw new NoSuchElementException(id + " is not an attribute");
		return attributes.get(id).value;
	}

	public List<Attribute<?>> getAttributes() {
		return new LinkedList<>(attributes.values());
	}

	@Override
	public String toString() {
		return "(" + getSource() + " : " + getTarget() + ") attributes=" + attributes;
	}
}
