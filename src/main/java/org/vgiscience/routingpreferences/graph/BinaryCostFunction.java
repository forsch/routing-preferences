package org.vgiscience.routingpreferences.graph;

import java.util.Objects;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;

public abstract class BinaryCostFunction implements LongCostFunction {

	private LongCostFunction masked;

	public BinaryCostFunction(LongCostFunction masked) {
		Objects.requireNonNull(masked);
		if (masked instanceof BinaryCostFunction)
			throw new IllegalArgumentException("masked cannot be of type BinaryCostFunction!");
		this.masked = masked;
	}

	public abstract boolean masked(AttributedEdge e);

	public LongCostFunction getMasked() {
		return masked;
	}

	@Override
	public Long apply(AttributedEdge e) {
		if (masked(e))
			return 0l;
		return masked.apply(e);
	}

	public Inverted inverse() {
		Inverted inverse = new Inverted(masked, this);
		return inverse;
	}

	@Override
	public String toString() {
		return "[BCF " + getClass().getSimpleName() + masked + "]";
	}

	public static class Cycling extends BinaryCostFunction {

		public Cycling(LongCostFunction masked) {
			super(masked);
		}

		@Override
		public boolean masked(AttributedEdge e) {
			return (boolean) e.getAttributeValue("cycling");
		}
	}

	public static class Inverted extends BinaryCostFunction {

		private BinaryCostFunction original;

		public Inverted(LongCostFunction masked, BinaryCostFunction nonInverted) {
			super(masked);
			this.original = nonInverted;
		}

		@Override
		public boolean masked(AttributedEdge e) {
			return !original.masked(e);
		}
	}
}
