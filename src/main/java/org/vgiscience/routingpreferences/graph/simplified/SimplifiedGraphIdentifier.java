package org.vgiscience.routingpreferences.graph.simplified;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;

public class SimplifiedGraphIdentifier {
	// original graph
	protected RoadGraph inputGraph;
	private Set<AttributedVertex> preservedVertices;

	// these maps map the arcs of the inputGraph to the arcs of the superGraph
	private Map<AttributedEdge, SimplifiedEdge> inputEdge2superEdge;

	public SimplifiedGraphIdentifier(RoadGraph inputGraph) {
		this.inputGraph = inputGraph;

		preservedVertices = new HashSet<>();
		inputEdge2superEdge = new HashMap<>();
	}

	protected void setPreserved(AttributedVertex v) {
		preservedVertices.add(v);
	}

	protected void setSuperArcOf(LinkedList<AttributedEdge> arcs, SimplifiedEdge sArc) {
		Objects.requireNonNull(arcs);
		Objects.requireNonNull(sArc);
		for (AttributedEdge arc : arcs) {
			inputEdge2superEdge.put(arc, sArc);
		}
	}

	public RoadGraph getInputGraph() {
		return inputGraph;
	}

	public SimplifiedEdge getSuperArcOf(AttributedEdge a) {
		return inputEdge2superEdge.get(a);
	}

	public boolean isPreserved(AttributedVertex n) {
		return preservedVertices.contains(n);
	}
}
