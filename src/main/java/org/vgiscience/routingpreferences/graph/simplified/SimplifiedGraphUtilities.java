package org.vgiscience.routingpreferences.graph.simplified;

import java.util.LinkedList;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;

/**
 * Provides utility functions to navigate and search graphs with eliminated
 * degree-two-nodes (so called Simplified Graphs). The connection between the
 * Input Graph and its Simplified Graph is given by a SimplifiedGraphIdentifier.
 * 
 * @author Axel Forsch
 *
 */
public class SimplifiedGraphUtilities {

	private SimplifiedGraphIdentifier sgi;

	public SimplifiedGraphUtilities(SimplifiedGraphIdentifier sgi) {
		this.sgi = sgi;
	}

	private RoadGraph inputGraph() {
		return sgi.inputGraph;
	}

	/**
	 * Returns the next super node along the degree-two-path starting at super node
	 * <code>superSource</code> with first arc to input node <code>first</code>.
	 * <p>
	 * All arcs along the degree-two-path to the returned node are added to the list
	 * <code>arcs</code>. The <code>identifier</code> is used to get the relation of
	 * input to super graph elements.
	 * 
	 * @param superSource super node to start search from
	 * @param first       first input node which defines degree-two-path
	 * @param arcs        container to collect all arcs of degree-two-path
	 * @return
	 */
	public AttributedVertex discoverArcs(AttributedVertex superSource, AttributedVertex first,
			LinkedList<AttributedEdge> arcs) {
		Collector visitor = new Collector();
		iterateOverArc(superSource, first, visitor);

		arcs.addAll(visitor.getArcs());
		return inputGraph().getEdgeTarget(arcs.getLast());
	}

	/**
	 * Iterates beginning a the super node <code>superSource</code> towards the next
	 * super node following the degree-two-path starting at the input node
	 * <code>first</code>.
	 * <p>
	 * On each input arc, the <code>visitor</code> performs a visit. For directed
	 * graphs, the tag <code>forward</code> specifies weather arcs are iterated from
	 * source to target (<code>forward == true</code>) or vice versa. The
	 * <code>identifier</code> is used to get the relation of input to super graph
	 * elements.
	 * 
	 * @param superSource super node where iteration begins
	 * @param first       first node on the degree-two-path to iterate
	 * @param visitor     visitor element to perform actions of path
	 * @param forward     direction of iteration (only for directed graphs)
	 * @param skipFirst   set true if the node "first" should be ignored while
	 *                    searching for a super node
	 */
	public void iterateOverArc(AttributedVertex superSource, AttributedVertex first, PathVisitor visitor,
			boolean skipFirst) {
		AttributedVertex s = superSource; // input node
		AttributedEdge curr;
		AttributedVertex nextNode;

		curr = inputGraph().getEdge(s, first);
		nextNode = inputGraph().getEdgeTarget(curr);

		if (!skipFirst) {
			if (!visitor.visit(curr)) {
				return;
			}
		}

		boolean isFirst = true;

		while (!sgi.isPreserved(nextNode) || (skipFirst && isFirst)) {

			for (AttributedEdge out : inputGraph().outgoingEdgesOf(nextNode)) {
				if (inputGraph().getEdgeTarget(out) != inputGraph().getEdgeSource(curr)) {
					curr = out;
					nextNode = inputGraph().getEdgeTarget(curr);
					break;
				}
			}

			if (!visitor.visit(curr)) {
				break;
			}
			isFirst = false;
		}
	}

	public void iterateOverArc(AttributedVertex superSource, AttributedVertex first, PathVisitor visitor) {
		iterateOverArc(superSource, first, visitor, false);
	}

	public AttributedVertex getVertexBeyond(AttributedVertex vertex, AttributedVertex antiVertex,
			LinkedList<AttributedEdge> arcs, boolean skipFirst) {
		VertexBeyondFinder nbf = new VertexBeyondFinder(vertex, antiVertex, skipFirst);
		arcs.addAll(nbf.getArcs());
		return nbf.getVertexBeyond();
	}

	/**
	 * Implementation of a path visitor that collects all visited arcs in an ordered
	 * list. If forward, newly visited arcs are added at the end of the list, else
	 * they are added at the beginning.
	 * 
	 * @author Johannes Oehrlein, Axel Forsch
	 *
	 * @param <V> data type of node data
	 * @param <E> data type of arc data
	 */
	public class Collector implements PathVisitor {

		private LinkedList<AttributedEdge> arcs;

		public Collector() {
			this.arcs = new LinkedList<>();
		}

		@Override
		public boolean visit(AttributedEdge arc) {
			arcs.add(arc);
			return true;
		}

		public LinkedList<AttributedEdge> getArcs() {
			return arcs;
		}
	}

	/**
	 * Helper class to insert paths into a super graph. In specific, this class
	 * helps by finding the super nodes the start and end points of the inserted
	 * path need to be connected to by an arc.
	 * <p>
	 * The inserted start and end points of the path are (most likely) inserted on
	 * an exisiting edge in the graph and thus form degree-two-nodes. They are
	 * connected to the graph to the one side by the inserted path, but to the other
	 * side they form a "loose end". The NodeBeyondFinder finds a node to connect
	 * this loose end to.
	 * 
	 * @author Johannes Oehrlein, Axel Forsch
	 *
	 */
	public class VertexBeyondFinder {

		private AttributedVertex vertexBeyond; // super node
		private AttributedVertex first; // input node
		private LinkedList<AttributedEdge> arcs; // input arcs

		public VertexBeyondFinder(AttributedVertex node, AttributedVertex antiNode, boolean skipFirst) {
			this.vertexBeyond = findVertexBeyond(node, antiNode, skipFirst);
		}

		public VertexBeyondFinder(AttributedVertex start, AttributedVertex first) {
			this(start, first, false);
		}

		public LinkedList<AttributedEdge> getArcs() {
			return arcs;
		}

		public AttributedVertex getFirst() {
			return first;
		}

		private AttributedVertex findVertexBeyond(AttributedVertex vertex, AttributedVertex antiVertex,
				boolean skipFirst) {
			this.first = vertex;

			FindNodeVisitor fnv = new FindNodeVisitor(vertex);
			if (skipFirst)
				fnv.i = 0;
			iterateOverArc(antiVertex, vertex, fnv, skipFirst);
			arcs = fnv.getArcs();
			return fnv.getTarget();
		}

		public AttributedVertex getVertexBeyond() {
			return vertexBeyond;
		}
	}

	/**
	 * Implementation of a path visitor that collects all arcs along a path but only
	 * returns them if the given target is reached.
	 * 
	 * @author Johannes Oehrlein, Axel Forsch
	 *
	 */
	protected class FindNodeVisitor implements PathVisitor {

		private AttributedVertex m;
		private Collector c;
		private int i;
		private int k;

		public FindNodeVisitor(AttributedVertex m) {
			this.m = m;
			c = new Collector();
			k = 0;
			i = -1;
		}

		@Override
		public boolean visit(AttributedEdge arc) {
			c.visit(arc);
			AttributedVertex curr = inputGraph().getEdgeTarget(arc);
			if (curr == m) {
				i = k;
			}
			++k;
			return true;
		}

		public LinkedList<AttributedEdge> getArcs() {
			if (found())
				return c.getArcs();
			return null;
		}

		public AttributedVertex getTarget() {
			if (found())
				return inputGraph().getEdgeTarget(getArcs().getLast());
			return null;
		}

		public AttributedVertex getSource() {
			if (found())
				return inputGraph().getEdgeSource(getArcs().getLast());
			return null;
		}

		public boolean found() {
			return i != -1;
		}

		public int getIndex() {
			return i;
		}

	}

	/**
	 * Interface to perform actions on arcs of the input graph given its super
	 * graph.
	 * 
	 * @author Johannes Oehrlein
	 *
	 * @param <V> type of node data
	 * @param <E> type of arc data
	 */
	public interface PathVisitor {
		public boolean visit(AttributedEdge arc);
	}
}
