package org.vgiscience.routingpreferences.graph.simplified;

import java.util.LinkedList;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.GraphWalk;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.path.RoadPath;

public class SimplifiedRoadPath extends GraphWalk<AttributedVertex, SimplifiedEdge> {

	private static final long serialVersionUID = -7430630314349483348L;

	protected String id;

	public SimplifiedRoadPath(String id, Graph<AttributedVertex, SimplifiedEdge> graph, AttributedVertex startVertex,
			AttributedVertex endVertex, List<AttributedVertex> vertexList, List<SimplifiedEdge> edgeList) {
		super(graph, startVertex, endVertex, vertexList, edgeList, 0);
		this.id = id;
		this.verify();
	}

	public SimplifiedRoadPath(String id, GraphPath<AttributedVertex, SimplifiedEdge> walk) {
		this(id, walk.getGraph(), walk.getStartVertex(), walk.getEndVertex(), walk.getVertexList(), walk.getEdgeList());
	}

	public RoadPath expand() {
		RoadGraph originalGraph = ((WeightedSimplifiedRoadGraph) graph).originalGraph();

		LinkedList<AttributedEdge> expandedEdges = new LinkedList<>();
		for (SimplifiedEdge e : edgeList) {
			for (AttributedEdge edge : e.simplifiedEdges) {
				if (!originalGraph.containsEdge(edge)) {
					LinkedList<AttributedEdge> replacementEdges = originalGraph.getReplacementEdgesFor(edge);
					for (AttributedEdge re : replacementEdges) {
						if (!originalGraph.containsEdge(re)) {
							// at most two replacements could happen: once for inserting start/end vertex of
							// path, once for inserting inbetween vertices
							expandedEdges.addAll(originalGraph.getReplacementEdgesFor(re));
						} else {
							expandedEdges.add(re);
						}
					}
				} else {
					expandedEdges.add(edge);
				}
			}
		}
		GraphWalk<AttributedVertex, AttributedEdge> walk = new GraphWalk<>(originalGraph, startVertex, endVertex,
				expandedEdges, weight);
		walk.verify();
		return new RoadPath(id, walk);
	}
}
