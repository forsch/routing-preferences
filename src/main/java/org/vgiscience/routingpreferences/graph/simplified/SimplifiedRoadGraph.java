package org.vgiscience.routingpreferences.graph.simplified;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.GraphWalk;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.path.PathInserter;
import org.vgiscience.routingpreferences.path.RoadPath;

import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class SimplifiedRoadGraph extends DirectedPseudograph<AttributedVertex, SimplifiedEdge> {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final long serialVersionUID = 4298726738626066727L;

	private SimplifiedGraphIdentifier sgi;
	private SimplifiedGraphUtilities utils;

	private HashSet<AttributedVertex> insertedVertices;
	private HashSet<SimplifiedEdge> insertedEdges;
	private HashSet<SimplifiedEdge> removedEdges;

	public SimplifiedRoadGraph(SimplifiedGraphIdentifier sgi) {
		super(SimplifiedEdge.class);
		this.sgi = sgi;
		this.utils = new SimplifiedGraphUtilities(sgi);
	}

	protected RoadGraph originalGraph() {
		return sgi.getInputGraph();
	}

	public <T, S> RoadPath insertPointListAsPath(IdentifiedPointList<T, S> pointList) {
		insertedVertices = new HashSet<>();
		insertedEdges = new HashSet<>();
		removedEdges = new HashSet<>();

		// step 1: insert path in original graph
		originalGraph().addEndpoints(pointList);
		GraphWalk<AttributedVertex, AttributedEdge> originalWalk = PathInserter.pointListAsWalk(pointList,
				originalGraph(), c -> new AttributedVertex(c));
		RoadPath path = originalGraph().insertInbetweenNodes(originalWalk,
				pointList.getTrackId() + "_" + pointList.getSubtrackId());

		// step 2: insert all vertices
		boolean predAdded = false;
		AttributedVertex pred = null;
		AttributedVertex predpred = null;
		for (AttributedVertex curr : path.getVertexList()) {
			// special case: u-turn in path on non-preserved vertex
			if (predAdded && curr.equals(predpred)) {
				logger.finer("Detected U-turn on non-preserved vertex " + pred);
				connectToNextPrevervedVertex(pred, predpred);
			}

			boolean currAdded = addVertex(curr);
			if (currAdded)
				insertedVertices.add(curr);

			if (pred != null) {
				if (getEdge(pred, curr) == null) { // arc does not already exists
					AttributedEdge e = originalGraph().getEdge(pred, curr);
					SimplifiedEdge newEdge = new SimplifiedEdge(e);
					addEdge(pred, curr, newEdge);
					insertedEdges.add(newEdge);

					// if arc is bidirectional, add its twin
					if (originalGraph().getEdge(curr, pred) != null) {
						newEdge = new SimplifiedEdge(originalGraph().getEdge(curr, pred));
						addEdge(curr, pred, newEdge);
						insertedEdges.add(newEdge);
					}
				}
			}
			predAdded = currAdded;
			predpred = pred;
			pred = curr;
		}

		if (!sgi.isPreserved(path.getStartVertex())) {
			logger.finer("source is not preserved -> connecting");
			connectToNextPrevervedVertex(path.getStartVertex(), path.getVertexList().get(1));
		}

		if (!sgi.isPreserved(path.getEndVertex())) {
			logger.finer("target is not preserved -> connecting");
			connectToNextPrevervedVertex(path.getEndVertex(), path.getVertexList().get(path.getLength() - 1));
		}

		return path;
	}

	/**
	 * Connects an additionally added vertex that is not connected by other path
	 * nodes to a preserved vertex (in specific, these can be: start and end vertex
	 * of a path and vertices forming a u-turn on a path) to the next preserved
	 * vertex.
	 * 
	 * @param vertexToConnect the vertex to connect to the next preserved vertex
	 * @param antiVertex      the vertex along the degree-two edge chain that
	 *                        indicates the opposite direction of search
	 * @param forward         (only for directed graphs) whether the arcs should be
	 *                        searched in forward or backward direction
	 */
	private void connectToNextPrevervedVertex(AttributedVertex vertexToConnect, AttributedVertex antiVertex) {
		LinkedList<AttributedEdge> inputArcs = new LinkedList<>();
		AttributedVertex vertexBeyond = utils.getVertexBeyond(vertexToConnect, antiVertex, inputArcs, true);

		if (getEdge(vertexToConnect, vertexBeyond) == null) { // if arc does not exist yet
			// calculate summed arc data
			SimplifiedEdge edge = new SimplifiedEdge(inputArcs);
			addEdge(vertexToConnect, vertexBeyond, edge);
			insertedEdges.add(edge);

			// reverse arc if needed
			if (originalGraph().hasTwin(inputArcs.getFirst())) {
				LinkedList<AttributedEdge> reverseInputArcs = new LinkedList<>();
				reverseInputArcs.add(originalGraph().getTwin(inputArcs.getFirst()));
				for (int i = 1; i < inputArcs.size(); i++) {
					if (!originalGraph().hasTwin(inputArcs.get(i))) {
						reverseInputArcs = null;
						break;
					}
					reverseInputArcs.addFirst(originalGraph().getTwin(inputArcs.get(i)));
				}
				if (reverseInputArcs != null) {
					SimplifiedEdge newEdge = new SimplifiedEdge(reverseInputArcs);
					addEdge(vertexBeyond, vertexToConnect, newEdge);
					insertedEdges.add(newEdge);
				}
			}
		}
	}

	public void restore() {
		if (insertedVertices == null && insertedEdges == null && removedEdges == null)
			return;

		for (SimplifiedEdge e : insertedEdges)
			this.removeEdge(e);
		for (SimplifiedEdge e : removedEdges)
			this.addEdge(this.getEdgeSource(e), this.getEdgeTarget(e), e);
		for (AttributedVertex v : insertedVertices)
			this.removeVertex(v);

		insertedVertices = null;
		insertedEdges = null;
		removedEdges = null;

		logger.info("Restored to n=" + this.vertexSet().size() + ", m=" + this.edgeSet().size());

		originalGraph().restore();
	}
}
