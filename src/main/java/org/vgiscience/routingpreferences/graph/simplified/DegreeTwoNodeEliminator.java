package org.vgiscience.routingpreferences.graph.simplified;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;

public class DegreeTwoNodeEliminator {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	// input: with degree 2 nodes, super: without degree 2 nodes (after simplify)
	private SimplifiedRoadGraph superGraph;
	private SimplifiedGraphIdentifier identifier;

	public DegreeTwoNodeEliminator(RoadGraph inputGraph) {
		this.identifier = new SimplifiedGraphIdentifier(inputGraph);
		this.simplify();
	}

	private RoadGraph inputGraph() {
		return identifier.inputGraph;
	}

	private boolean preserveVertex(AttributedVertex v) {
		if (identifier.inputGraph.inDegreeOf(v) == 2 && identifier.inputGraph.outDegreeOf(v) == 2) {
			return false;
		}
		return true;
	}

	private void simplify() {
		superGraph = new SimplifiedRoadGraph(identifier);

		for (AttributedVertex n : identifier.inputGraph.vertexSet()) {
			if (preserveVertex(n)) {
				superGraph.addVertex(n);
				identifier.setPreserved(n);
			}
		}

		SimplifiedGraphUtilities utils = new SimplifiedGraphUtilities(identifier);

		ArrayList<AttributedVertex> vertexSet = new ArrayList<>(superGraph.vertexSet());
		for (int i = 0; i < vertexSet.size(); ++i) {
			AttributedVertex sn = vertexSet.get(i);
			AttributedVertex n = sn;

			// for every arc in the input graph starting at an preserved node, find the
			// first preserved node along the path started by it (e.g. find the first
			// non-degree-two-node in this direction). Add an super arc to the found node.
			for (AttributedEdge out : inputGraph().outgoingEdgesOf(n)) {
				AttributedVertex first = inputGraph().getEdgeTarget(out);

				// collect arcs along degree-two-path and store its target
				LinkedList<AttributedEdge> arcs = new LinkedList<>();
				AttributedVertex target = utils.discoverArcs(sn, first, arcs);
				AttributedVertex superTarget = target;

				// create superArc to this target with accumulated edge weights
				SimplifiedEdge superEdge = new SimplifiedEdge(arcs);
				superGraph.addEdge(sn, superTarget, superEdge);
				identifier.setSuperArcOf(arcs, superEdge);
			}
		}
		logger.info("Simplification done");
	}

	public SimplifiedRoadGraph getSimplifiedGraph() {
		return superGraph;
	}
}
