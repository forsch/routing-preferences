package org.vgiscience.routingpreferences.graph.simplified;

import java.util.function.Function;

import org.jgrapht.graph.AsWeightedGraph;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.DoubleCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;

public class WeightedSimplifiedRoadGraph extends AsWeightedGraph<AttributedVertex, SimplifiedEdge> {

	private static final long serialVersionUID = 2528874108706789732L;

	public WeightedSimplifiedRoadGraph(SimplifiedRoadGraph graph, DoubleCostFunction basicCostFunction) {
		super(graph, new SimplifiedDoubleCostFunction(basicCostFunction), false, false);
	}

	public RoadGraph originalGraph() {
		return ((SimplifiedRoadGraph) getDelegate()).originalGraph();
	}

	public static class SimplifiedDoubleCostFunction implements Function<SimplifiedEdge, Double> {

		private DoubleCostFunction basicCostFunction;

		public SimplifiedDoubleCostFunction(DoubleCostFunction basicCostFunction) {
			this.basicCostFunction = basicCostFunction;
		}

		@Override
		public Double apply(SimplifiedEdge t) {
			double w = 0;
			for (AttributedEdge e : t.simplifiedEdges) {
				w += basicCostFunction.apply(e);
			}
			return w;
		}
	}
}
