package org.vgiscience.routingpreferences.graph;

import java.util.Objects;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;

public class CombinedDoubleCostFunction implements DoubleCostFunction {

	private double alpha;
	private LongCostFunction primary;
	private LongCostFunction secondary;

	public CombinedDoubleCostFunction(double alpha, LongCostFunction primary, LongCostFunction secondary) {
		Objects.requireNonNull(primary);
		Objects.requireNonNull(secondary);
		if (alpha < 0 || alpha > 1)
			throw new IllegalArgumentException("alpha must be in interval [0,1], got: " + alpha);
		if (primary instanceof BinaryCostFunction)
			throw new IllegalArgumentException("primary cannot be a binary cost function");
		if (secondary instanceof BinaryCostFunction && ((BinaryCostFunction) secondary).getMasked() != primary)
			throw new IllegalArgumentException("masked of secondary must be primary!");
		this.alpha = alpha;
		this.primary = primary;
		this.secondary = secondary;

		if (secondary instanceof BinaryCostFunction)
			this.primary = ((BinaryCostFunction) secondary).inverse();
	}

	public CombinedDoubleCostFunction(double alpha, BinaryCostFunction secondary) {
		Objects.requireNonNull(secondary);
		if (alpha < 0 || alpha > 1)
			throw new IllegalArgumentException("alpha must be in interval [0,1], got: " + alpha);
		this.alpha = alpha;
		this.primary = secondary.inverse();
		this.secondary = secondary;
	}

	@Override
	public Double apply(AttributedEdge e) {
		long p = primary.apply(e);
		long s = secondary.apply(e);
		return (1 - alpha) * p + alpha * s;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
}
