package org.vgiscience.routingpreferences.graph;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;

public abstract class FactoredCostFunction implements LongCostFunction {

	public static final String FACTOR_NAME = "factor";

	@Override
	public final Long apply(AttributedEdge t) {
		double factor = 2.0;
		if (t.hasAttribute(FACTOR_NAME))
			factor = (Double) t.getAttributeValue(FACTOR_NAME);
		return Math.round(factor * applyUnfactored(t));
	}

	public abstract long applyUnfactored(AttributedEdge t);

	@Override
	public String toString() {
		return "[FCF " + getClass().getSimpleName() + "]";
	}

	public static final class Distance extends FactoredCostFunction {

		public static final String ATTRIBUTE_NAME = "length";
		public static final double DOUBLE_TO_LONG_FACTOR = 1000;

		@Override
		public long applyUnfactored(AttributedEdge t) {
			if (!t.hasAttribute(ATTRIBUTE_NAME))
				throw new RuntimeException("edge does not have attribute " + ATTRIBUTE_NAME);
			return Math.round((double) t.getAttributeValue(ATTRIBUTE_NAME) * DOUBLE_TO_LONG_FACTOR);
		}

	};

	public static final class Ascent extends FactoredCostFunction {

		public static final String ATTRIBUTE_NAME = "ascent";
		public static final double DOUBLE_TO_LONG_FACTOR = 1000;

		@Override
		public long applyUnfactored(AttributedEdge t) {
			if (!t.hasAttribute(ATTRIBUTE_NAME))
				throw new RuntimeException("edge does not have attribute " + ATTRIBUTE_NAME);
			return Math.round((double) t.getAttributeValue(ATTRIBUTE_NAME) * DOUBLE_TO_LONG_FACTOR);
		}
	};

}
