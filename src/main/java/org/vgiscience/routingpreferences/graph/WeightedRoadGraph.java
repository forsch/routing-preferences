package org.vgiscience.routingpreferences.graph;

import org.jgrapht.graph.AsWeightedGraph;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;

public class WeightedRoadGraph extends AsWeightedGraph<AttributedVertex, AttributedEdge> {

	private static final long serialVersionUID = -2431803927564639325L;

	public WeightedRoadGraph(RoadGraph graph, DoubleCostFunction weightFunction) {
		super(graph, weightFunction, false, false);
	}
}
