package org.vgiscience.routingpreferences.graph;

import java.util.HashMap;
import java.util.List;

import org.vgiscience.routingpreferences.datatypes.Attribute;
import org.vgiscience.routingpreferences.datatypes.AttributeFactory;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;

import de.geoinfobonn.io.model.graph.FeatureEdge;
import de.geoinfobonn.io.model.graph.FeatureGraph;
import de.geoinfobonn.io.model.graph.FeatureVertex;

public class FeatureGraphConverter {

	public static RoadGraph toRoadGraph(FeatureGraph fg, List<AttributeFactory<?>> factories) {
		RoadGraph g = new RoadGraph();

		HashMap<FeatureVertex, AttributedVertex> nodeMap = new HashMap<>();
		for (FeatureVertex fv : fg.vertexSet()) {
			AttributedVertex av = new AttributedVertex(fv.getLocation());
			if (fv.hasFeature() && fv.getFeature().hasAttribute("height"))
				av.addAttribute(new Attribute<Double>("height", (Double) fv.getFeature().getAttribute("height")));
			boolean added = g.addVertex(av);
			if (!added)
				throw new RuntimeException("Duplicate node " + av);
			nodeMap.put(fv, av);
		}

		for (FeatureEdge fe : fg.edgeSet()) {
			AttributedEdge ae = new AttributedEdge();
			FeatureVertex source = fg.getEdgeSource(fe);
			FeatureVertex target = fg.getEdgeTarget(fe);

			if (factories != null)
				for (AttributeFactory<?> factory : factories) {
					Attribute<?> a = factory.getAttribute(fe, source, target);
					ae.addAttribute(a);
				}

			boolean added = g.addEdge(nodeMap.get(source), nodeMap.get(target), ae);
			if (!added)
				throw new RuntimeException("Duplicate arc " + ae);
		}

		return g;
	}
}
