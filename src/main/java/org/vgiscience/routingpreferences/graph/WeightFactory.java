package org.vgiscience.routingpreferences.graph;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;

public interface WeightFactory {

	public double calculateWeight(AttributedEdge edge);
}
