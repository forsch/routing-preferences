package org.vgiscience.routingpreferences.graph;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.jgrapht.graph.GraphWalk;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineSegment;
import org.locationtech.jts.index.kdtree.KdNode;
import org.locationtech.jts.index.kdtree.KdNodeVisitor;
import org.locationtech.jts.index.kdtree.KdTree;
import org.vgiscience.routingpreferences.datatypes.Attribute;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.datatypes.DoubleAttribute;
import org.vgiscience.routingpreferences.path.RoadPath;

import de.geoinfobonn.io.FeatureReader;
import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class RoadGraph extends SimpleDirectedGraph<AttributedVertex, AttributedEdge> {
	private static Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	private static final long serialVersionUID = 6920881618305283693L;
	private static final int MAX_SEARCH_POWER = 10;
	private static final double EPS = 1e-3;

	private HashSet<AttributedVertex> insertedVertices;
	private HashMap<AttributedEdge, LinkedList<AttributedEdge>> replacedEdges;

	private KdTree index;

	public RoadGraph() {
		super(AttributedEdge.class);
		index = new KdTree();
	}

	public <T, S> void addEndpoints(IdentifiedPointList<T, S> pointList) {
		LinkedList<Coordinate> pts = pointList.getUniqueTrackPoints();

		// find first and last vertex in path that are in the graph
		AttributedVertex first = null, last = null;
		for (Coordinate c : pts) {
			AttributedVertex v = new AttributedVertex(c);
			if (this.containsVertex(v)) {
				first = v;
				break;
			}
		}

		if (first == null)
			throw new RuntimeException("Track " + pointList + " is on a single arc of the graph!");

		for (int i = pts.size() - 1; i >= 0; i--) {
			AttributedVertex v = new AttributedVertex(pts.get(i));
			if (this.containsVertex(v)) {
				last = v;
				break;
			}
		}

		// handle source
		AttributedVertex sv = new AttributedVertex(pts.getFirst());
		if (!this.containsVertex(sv)) {
			for (AttributedEdge e : this.incomingEdgesOf(first)) {
				AttributedVertex s = this.getEdgeSource(e);
				LineSegment seg = new LineSegment(s.getLocation(), first.getLocation());
				if (seg.distance(sv.getLocation()) < EPS) {
					addAdditionalVertexOnEdge(sv, Optional.of(e));
					logger.finer("source added");
					break;
				}
			}
			if (!this.containsVertex(sv))
				throw new RuntimeException("unable to insert source");
		}

		// handle target
		AttributedVertex tv = new AttributedVertex(pts.getLast());
		if (!this.containsVertex(tv)) {
			for (AttributedEdge e : this.outgoingEdgesOf(last)) {
				AttributedVertex t = this.getEdgeTarget(e);
				LineSegment seg = new LineSegment(last.getLocation(), t.getLocation());
				if (seg.distance(tv.getLocation()) < EPS) {
					addAdditionalVertexOnEdge(tv, Optional.of(e));
					logger.finer("target added");
					break;
				}
			}
			if (!this.containsVertex(tv))
				throw new RuntimeException("unable to insert target");
		}
	}

	public RoadPath insertInbetweenNodes(GraphWalk<AttributedVertex, AttributedEdge> walk, String id) {
		if (walk.getGraph() != this)
			throw new IllegalArgumentException("graph of walk not this graph!");

		if (insertedVertices == null && replacedEdges == null) {
			insertedVertices = new HashSet<>();
			replacedEdges = new HashMap<>();
		}

		LinkedList<AttributedVertex> newVertices = new LinkedList<>();
		LinkedList<AttributedEdge> newEdges = new LinkedList<>();
		for (AttributedEdge e : walk.getEdgeList()) {
			// create in-between vertex
			AttributedVertex source = this.getEdgeSource(e);
			AttributedVertex target = this.getEdgeTarget(e);
			Coordinate midpoint = LineSegment.midPoint(source.getLocation(), target.getLocation());
			FeatureReader.PM.makePrecise(midpoint);
			AttributedVertex midvertex = new AttributedVertex(midpoint);
			newVertices.add(source);
			newVertices.add(midvertex);

			boolean added = this.addVertex(midvertex);
			if (!added) {
				newEdges.add(this.getEdge(source, midvertex));
				newEdges.add(this.getEdge(midvertex, target));
				logger.finest("already splitted " + midvertex);
				continue;
			}
			insertedVertices.add(midvertex);

			// create new edges
			LinkedList<AttributedEdge> currentNewEdges = generateSplitEdges(e, midvertex);
			newEdges.addAll(currentNewEdges);
			this.removeEdge(e);
			replacedEdges.put(e, currentNewEdges);

			// generate twin
			if (this.containsEdge(target, source)) {
				AttributedEdge twin = this.getEdge(target, source);
				currentNewEdges = generateSplitEdges(twin, midvertex);
				this.removeEdge(twin);
				replacedEdges.put(twin, currentNewEdges);
			}
		}
		newVertices.add(walk.getEndVertex());

		return new RoadPath(id, this, newVertices.getFirst(), newVertices.getLast(), newVertices, newEdges);
	}

	public void restore() {
		if (insertedVertices == null && replacedEdges == null)
			return;

		for (LinkedList<AttributedEdge> insertedEdges : replacedEdges.values())
			for (AttributedEdge e : insertedEdges)
				this.removeEdge(e);
		for (AttributedEdge e : replacedEdges.keySet())
			this.addEdge(this.getEdgeSource(e), this.getEdgeTarget(e), e);
		for (AttributedVertex v : insertedVertices)
			this.removeVertex(v);

		insertedVertices = null;
		replacedEdges = null;

		logger.info("Restored to n=" + this.vertexSet().size() + ", m=" + this.edgeSet().size());
	}

	private boolean addAdditionalVertexOnEdge(AttributedVertex v, Optional<AttributedEdge> e) {
		if (insertedVertices == null && replacedEdges == null) {
			insertedVertices = new HashSet<>();
			replacedEdges = new HashMap<>();
		}
		if (!this.containsVertex(v)) {
			this.addVertex(v);
			insertedVertices.add(v);

			AttributedVertex s = e.map(this::getEdgeSource).orElse(null);
			AttributedVertex t = e.map(this::getEdgeTarget).orElse(null);
			if (e.isEmpty()) {
				for (AttributedEdge edge : this.edgeSet()) {
					s = this.getEdgeSource(edge);
					t = this.getEdgeTarget(edge);
					LineSegment seg = new LineSegment(s.getLocation(), t.getLocation());
					if (seg.distance(v.getLocation()) < 1e-6) {
						e = Optional.of(edge);
						break;
					}
				}
				if (e.isEmpty())
					throw new RuntimeException("no edge containing vertex found!");
			}

			LinkedList<AttributedEdge> currentNewEdges = generateSplitEdges(e.get(), v);
			this.removeEdge(e.get());
			replacedEdges.put(e.get(), currentNewEdges);
			if (this.containsEdge(t, s)) {
				AttributedEdge er = this.getEdge(t, s);
				currentNewEdges = generateSplitEdges(er, v);
				this.removeEdge(er);
				replacedEdges.put(er, currentNewEdges);
			}
			return true;
		}
		return false;
	}

	private LinkedList<AttributedEdge> generateSplitEdges(AttributedEdge e, AttributedVertex splitVertex) {
		AttributedVertex source = this.getEdgeSource(e);
		AttributedVertex target = this.getEdgeTarget(e);

		double fraction = source.getLocation().distance(splitVertex.getLocation())
				/ source.getLocation().distance(target.getLocation());
		double oldFactor = !e.hasAttribute(FactoredCostFunction.FACTOR_NAME) ? 2.0
				: (double) e.getAttributeValue(FactoredCostFunction.FACTOR_NAME);

		// create new edges
		AttributedEdge e1 = this.addEdge(source, splitVertex);
		AttributedEdge e2 = this.addEdge(splitVertex, target);
		for (Attribute<?> attribute : e.getAttributes()) {
			if (attribute.getId().equals(FactoredCostFunction.FACTOR_NAME))
				continue;
			e1.addAttribute(attribute);
			e2.addAttribute(attribute);
		}
		e1.addAttribute(new DoubleAttribute(FactoredCostFunction.FACTOR_NAME, oldFactor * fraction));
		e2.addAttribute(new DoubleAttribute(FactoredCostFunction.FACTOR_NAME, oldFactor * (1 - fraction)));

		LinkedList<AttributedEdge> newEdges = new LinkedList<>();
		newEdges.add(e1);
		newEdges.add(e2);
		return newEdges;
	}

	@Override
	public boolean addVertex(AttributedVertex v) {
		boolean added = super.addVertex(v);
		if (added) {
			KdNode newNode = index.insert(v.getLocation(), v);
			if (newNode == null)
				logger.severe("node already present at " + v.getLocation() + "(" + v + ")");
		}
		return added;
	}

	public AttributedVertex getNodeAt(Coordinate location) {
		double radius = 2;
		for (int i = 0; i <= MAX_SEARCH_POWER; ++i) {
			AttributedVertex vertex = getNodeAt(location, Math.pow(radius, i));
			if (vertex != null)
				return vertex;
		}
		logger.warning("No vertex found in radius " + Math.pow(radius, MAX_SEARCH_POWER) + " around " + location);
		return null;
	}

	public AttributedVertex getNodeAt(Coordinate location, double radius) {
		Envelope queryEnv = new Envelope(location.x - radius, location.x + radius, location.y - radius,
				location.y + radius);
		NodeCollector collector = new NodeCollector(location, radius);
		index.query(queryEnv, collector);

		Entry<Double, KdNode> nearestNode = collector.getNearest();
		if (nearestNode == null)
			return null;

		AttributedVertex vertex = (AttributedVertex) nearestNode.getValue().getData();
		logger.finer("Found " + vertex + " in a distance of " + nearestNode.getKey() + " from the query location.");
		return vertex;
	}

	private static class NodeCollector implements KdNodeVisitor {

		private Coordinate origin;
		private double radius;
		private TreeMap<Double, KdNode> foundNodes;

		public NodeCollector(Coordinate origin, double radius) {
			super();
			this.origin = origin;
			this.radius = radius;
			this.foundNodes = new TreeMap<>();
		}

		public Entry<Double, KdNode> getNearest() {
			return foundNodes.firstEntry();
		}

		@Override
		public void visit(KdNode node) {
			double dist = node.getCoordinate().distance(origin);
			if (dist < radius)
				foundNodes.put(dist, node);
		}
	}

	public AttributedEdge getTwin(AttributedEdge e) {
		Objects.requireNonNull(e);
		if (!containsEdge(e))
			throw new IllegalArgumentException("Edge " + e + " not part of graph");

		return getEdge(getEdgeTarget(e), getEdgeSource(e));
	}

	public boolean hasTwin(AttributedEdge e) {
		return getTwin(e) != null;
	}

	public LinkedList<AttributedEdge> getReplacementEdgesFor(AttributedEdge e) {
		Objects.requireNonNull(e);
		if (!replacedEdges.containsKey(e))
			throw new IllegalArgumentException(e + " is not a replaced arc.");
		return replacedEdges.get(e);
	}

	public LinkedList<AttributedEdge> getReplacementEdgesFor(AttributedVertex s, AttributedVertex t) {
		Objects.requireNonNull(s);
		Objects.requireNonNull(t);
		for (AttributedEdge e : getReplacedEdges()) {
			if (getEdgeSource(e).equals(s) && getEdgeTarget(e).equals(t)) {
				return replacedEdges.get(e);
			}
		}
		throw new IllegalArgumentException("there is not a replaced arc for (" + s + " -> " + t + ")");
	}

	public LinkedList<AttributedEdge> getReplacedEdges() {
		return new LinkedList<>(replacedEdges.keySet());
	}
}
