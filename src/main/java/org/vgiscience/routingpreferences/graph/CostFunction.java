package org.vgiscience.routingpreferences.graph;

import java.util.function.Function;

import org.vgiscience.routingpreferences.datatypes.AttributedEdge;

public interface CostFunction<E extends Number> extends Function<AttributedEdge, E> {

}
