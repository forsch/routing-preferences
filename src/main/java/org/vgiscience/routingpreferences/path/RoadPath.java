package org.vgiscience.routingpreferences.path;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.NotImplementedException;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.GraphWalk;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.vgiscience.orfinder.exceptions.AlphaOutsideIntervalException;
import org.vgiscience.orfinder.exceptions.BoundaryLineNotOptimalException;
import org.vgiscience.orfinder.exceptions.InvalidIntervalException;
import org.vgiscience.orfinder.exceptions.MaximumIterationException;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.routingpreferences.bicriterial.IntervalMatrixArray;
import org.vgiscience.routingpreferences.bicriterial.ResultSet;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.LongCostFunction;

public class RoadPath extends GraphWalk<AttributedVertex, AttributedEdge> {

	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
	private static final long serialVersionUID = 9083495182796346282L;

	private IntervalMatrixArray ima;
	private String id;

	public RoadPath(String id, Graph<AttributedVertex, AttributedEdge> graph, AttributedVertex startVertex,
			AttributedVertex endVertex, List<AttributedVertex> vertexList, List<AttributedEdge> edgeList) {
		super(graph, startVertex, endVertex, vertexList, edgeList, 0);
		this.id = id;
		this.verify();
	}

	public RoadPath(String id, GraphPath<AttributedVertex, AttributedEdge> walk) {
		this(id, walk.getGraph(), walk.getStartVertex(), walk.getEndVertex(), walk.getVertexList(), walk.getEdgeList());
	}

	public ResultSet decompose(RoadPathLineProvider lineProvider, double minAlpha) throws MaximumIterationException,
			InvalidIntervalException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		if (minAlpha >= 0.5)
			throw new IllegalArgumentException("minAlpha must be smaller than 0.5!");
		Interval searchInterval = new Interval(0 + minAlpha, 1 - minAlpha);
		ima = new IntervalMatrixArray(lineProvider, this, searchInterval);
		ima.decompose();
		ResultSet rs = new ResultSet(id, new ArrayList<>(getVertexList()), ima.getSuites(),
				ima.computeMultipleAlphaDecomposition(), searchInterval);
		return rs;
	}

//	public ResultSet decompose(BinaryCostFunction secondary, double minAlpha) throws MaximumIterationException,
//			InvalidIntervalException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
//		if (minAlpha >= 0.5)
//			throw new IllegalArgumentException("minAlpha must be smaller than 0.5!");
//		Interval searchInterval = new Interval(0 + minAlpha, 1 - minAlpha);
//		ima = new IntervalMatrixArray(new RoadPathLineProvider((RoadGraph) this.getGraph(), this, secondary), this,
//				searchInterval);
//		ima.decompose();
//		ResultSet rs = new ResultSet(id, new ArrayList<>(getVertexList()), ima.getSuites(),
//				ima.computeMultipleAlphaDecomposition(), searchInterval);
//		return rs;
//	}

	public RoadPath subPath(int c, int i) {
		RoadPath subPath = new RoadPath(id + "_c-i", //
				this.graph, //
				this.vertexList.get(c), //
				this.vertexList.get(i - 1), //
				this.vertexList.subList(c, i), //
				this.edgeList.subList(c, i - 1));
		subPath.verify();
		return subPath;
	}

	@Override
	public double getWeight() {
		throw new NotImplementedException("use evaluate(CostFunction) instead");
	}

	public long evaluate(LongCostFunction cf) {
		long weight = 0;
		for (AttributedEdge e : getEdgeList())
			weight += cf.apply(e);
		return weight;
	}

	/**
	 * Returns the geometric length of this path.
	 * 
	 * @return geometric length
	 */
	public double length() {
		return toLineString().getLength();
	}

	public LineString toLineString() {
		GeometryFactory gf = JTSFactoryFinder.getGeometryFactory();
		Coordinate[] c = vertexList.stream().map(v -> v.getLocation()).toArray(Coordinate[]::new);
		return gf.createLineString(c);
	}

	public IntervalMatrixArray getIntervalMatrixArray() {
		return ima;
	}

	public void exportToCSV(File file, LongCostFunction primary, LongCostFunction secondary) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write("id;x;y;p;s;");
			bw.newLine();
			for (int i = 0; i <= this.getLength(); ++i) {
				if (i == 0) {
					bw.write(writeVertex(i, this.getStartVertex()));
					bw.write(";;");
					bw.newLine();
				} else {
					AttributedVertex target = vertexList.get(i);
					AttributedEdge edge = edgeList.get(i - 1);

					bw.write(writeVertex(i, target));
					bw.write(primary.apply(edge) + ";");
					bw.write(secondary.apply(edge) + ";");
					bw.newLine();
				}
			}
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
	}

	private static String writeVertex(int i, AttributedVertex v) {
		return i + ";" + v.getLocation().getX() + ";" + v.getLocation().getY() + ";";
	}

	public String getId() {
		return id;
	}
}
