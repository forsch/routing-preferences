package org.vgiscience.routingpreferences.path;

import java.lang.invoke.MethodHandles;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.logging.Logger;

import org.jgrapht.Graph;
import org.jgrapht.graph.GraphWalk;
import org.locationtech.jts.geom.Coordinate;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.RoadGraph;

import de.geoinfobonn.io.model.path.IdentifiedPointList;

public class PathInserter {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	@SuppressWarnings("unchecked")
	public static <V, E> GraphWalk<V, E> pointListAsWalk(IdentifiedPointList<?, ?> points, Graph<V, E> g,
			Function<Coordinate, V> vertexFunction) {
		LinkedList<Coordinate> trackPoints = points.getUniqueTrackPoints();
		LinkedList<V> vertexList = new LinkedList<>();
		LinkedList<E> edgeList = new LinkedList<>();

		V source = vertexFunction.apply(trackPoints.getFirst());
		if (!g.containsVertex(source))
			logger.warning("first point not in graph --> skipped");

		V target = vertexFunction.apply(trackPoints.getLast());
		if (!g.containsVertex(target))
			logger.warning("last point not in graph --> skipped");

		V prev = null;
		double weight = 0;
		for (Coordinate c : trackPoints) {
			V v = vertexFunction.apply(c);
			if (g.containsVertex(v)) {
				if (v.equals(prev)) {
					logger.finer("duplicate node in path (" + v + ")");
					continue;
				}
				if (prev != null) {
					if (!g.containsEdge(prev, v)) {
						if (g instanceof RoadGraph) {
							RoadGraph rg = (RoadGraph) g;
							LinkedList<AttributedEdge> replacedEdges = rg
									.getReplacementEdgesFor((AttributedVertex) prev, (AttributedVertex) v);

							vertexList.add((V) rg.getEdgeTarget(replacedEdges.getFirst()));
							for (AttributedEdge edge : replacedEdges) {
								weight += g.getEdgeWeight((E) edge);
								edgeList.add((E) edge);
							}
						} else {
							throw new RuntimeException("no edge found! (" + prev + " -> " + v + ")");
						}
					} else {
						E edge = g.getEdge(prev, v);
						weight += g.getEdgeWeight(edge);
						edgeList.add(edge);
					}
				}
				vertexList.add(v);
				prev = v;
			}
		}

		return new GraphWalk<>(g, vertexList.getFirst(), vertexList.getLast(), vertexList, edgeList, weight);
	}
}
