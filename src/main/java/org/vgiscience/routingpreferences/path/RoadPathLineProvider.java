package org.vgiscience.routingpreferences.path;

import java.util.Objects;
import java.util.Optional;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.BidirectionalDijkstraShortestPath;
import org.jgrapht.graph.AsWeightedGraph;
import org.vgiscience.orfinder.linearrangement.Line;
import org.vgiscience.orfinder.linearrangement.LongLine;
import org.vgiscience.orfinder.linearrangement.LongLineProvider;
import org.vgiscience.orfinder.structures.QueryStat;
import org.vgiscience.routingpreferences.datatypes.AttributedEdge;
import org.vgiscience.routingpreferences.datatypes.AttributedVertex;
import org.vgiscience.routingpreferences.graph.BinaryCostFunction;
import org.vgiscience.routingpreferences.graph.CombinedDoubleCostFunction;
import org.vgiscience.routingpreferences.graph.LongCostFunction;
import org.vgiscience.routingpreferences.graph.RoadGraph;
import org.vgiscience.routingpreferences.graph.WeightedRoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedEdge;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedRoadGraph;
import org.vgiscience.routingpreferences.graph.simplified.SimplifiedRoadPath;
import org.vgiscience.routingpreferences.graph.simplified.WeightedSimplifiedRoadGraph;

public class RoadPathLineProvider extends LongLineProvider<RoadPath> {

	private AsWeightedGraph<AttributedVertex, ?> wrg;
	private LongCostFunction primary;
	private LongCostFunction secondary;

	private CombinedDoubleCostFunction combined;
	private ShortestPathAlgorithm<AttributedVertex, ?> sp;

	public RoadPathLineProvider(RoadGraph rg, RoadPath queryPath, LongCostFunction primary,
			LongCostFunction secondary) {
		this.query = Objects.requireNonNull(queryPath);
		this.secondary = Objects.requireNonNull(secondary);

		if (primary instanceof BinaryCostFunction)
			throw new IllegalArgumentException("primary cannot be a binary cost function");
		if (secondary instanceof BinaryCostFunction && ((BinaryCostFunction) secondary).getMasked() != primary)
			throw new IllegalArgumentException("masked of secondary must be primary!");

		if (secondary instanceof BinaryCostFunction) {
			this.primary = ((BinaryCostFunction) secondary).inverse();
			this.combined = new CombinedDoubleCostFunction(0, (BinaryCostFunction) secondary);
		} else {
			this.primary = primary;
			this.combined = new CombinedDoubleCostFunction(0, primary, secondary);
		}

		this.wrg = new WeightedRoadGraph(rg, combined);
		this.sp = new BidirectionalDijkstraShortestPath<>(wrg);
	}

	public RoadPathLineProvider(RoadGraph rg, RoadPath queryPath, BinaryCostFunction secondary) {
		this.query = queryPath;
		this.primary = secondary.inverse();
		this.secondary = secondary;
		this.combined = new CombinedDoubleCostFunction(0, secondary);

		this.wrg = new WeightedRoadGraph(rg, combined);
		this.sp = new BidirectionalDijkstraShortestPath<>(wrg);
	}

	public RoadPathLineProvider(SimplifiedRoadGraph sg, RoadPath queryPath, LongCostFunction primary,
			LongCostFunction secondary) {
		this.query = Objects.requireNonNull(queryPath);
		this.secondary = Objects.requireNonNull(secondary);

		if (primary instanceof BinaryCostFunction)
			throw new IllegalArgumentException("primary cannot be a binary cost function");
		if (secondary instanceof BinaryCostFunction && ((BinaryCostFunction) secondary).getMasked() != primary)
			throw new IllegalArgumentException("masked of secondary must be primary!");

		if (secondary instanceof BinaryCostFunction) {
			this.primary = ((BinaryCostFunction) secondary).inverse();
			this.combined = new CombinedDoubleCostFunction(0, (BinaryCostFunction) secondary);
		} else {
			this.primary = primary;
			this.combined = new CombinedDoubleCostFunction(0, primary, secondary);
		}

		this.wrg = new WeightedSimplifiedRoadGraph(sg, combined);
		this.sp = new BidirectionalDijkstraShortestPath<>(wrg);
	}

	@Override
	protected Line<Long, RoadPath> generateQueryLine(RoadPath query) {
		return toLine(query);
	}

	@Override
	protected Line<Long, RoadPath> generateLineAtLowerEnvelope(double alpha, Optional<QueryStat> stat) {
		AttributedVertex source = query.getStartVertex();
		AttributedVertex target = query.getEndVertex();

		RoadPath path = findShortestPath(source, target, alpha, stat);
		return toLine(path);
	}

	@SuppressWarnings("unchecked")
	public RoadPath findShortestPath(AttributedVertex source, AttributedVertex target, double alpha,
			Optional<QueryStat> stat) {
		setAlpha(alpha);

		long t = System.currentTimeMillis();
		// call Dijkstra's algorithm
		RoadPath path;
		if (wrg instanceof WeightedSimplifiedRoadGraph) {
			GraphPath<AttributedVertex, SimplifiedEdge> walk = (GraphPath<AttributedVertex, SimplifiedEdge>) sp
					.getPath(source, target);
			SimplifiedRoadPath simplePath = new SimplifiedRoadPath("sp_alpha" + alpha, walk);
			path = simplePath.expand();
		} else {
			GraphPath<AttributedVertex, AttributedEdge> walk = (GraphPath<AttributedVertex, AttributedEdge>) sp
					.getPath(source, target);
			path = new RoadPath("sp_alpha" + alpha, walk);
		}
		stat.ifPresent(s -> s.addDijkstraTime(System.currentTimeMillis() - t));

		return path;
	}

	private LongLine<RoadPath> toLine(RoadPath path) {
		long p = path.evaluate(primary);
		long s = path.evaluate(secondary);

		LongLine<RoadPath> line = new LongLine<>(p, s, path);
		return line;
	}

	private void setAlpha(double alpha) {
		combined.setAlpha(alpha);
	}
}
